# TODO
- [ ] Moar unit testing:
  - [ ] Inspectors
- [ ] TypeScript models for Blockcerts / Cowcerts JSONLD entities
- [ ] Validate documents using JSON Schemas
- [ ] Validate using Blockcerts v1.2
- [ ] Validate multiple anchors, not just one

## Tools
- https://github.com/typestack/class-transformer/
- https://github.com/typestack/class-validator
