#!/bin/sh
set -e

# Arguments
ACTION="$1"

# Methods
app_install_deps() {
    npm ci 2>&1
}

app_bundle() {
    npm run build 2>&1
}

app_lint() {
    npm run lint 2>&1
}

app_unittest() {
    npm run unittest 2>&1
}

# Switch action and perform
case "$ACTION" in
    "build")
        app_install_deps
        app_bundle
        ;;
    "lint")
        app_install_deps
        app_lint
        ;;
    "unittest")
        app_install_deps
        app_unittest
        ;;
    # No action
    "")
        echo "CI action to perform must be specified"
        echo "See .ci/action.sh for available actions"
        exit 1
        ;;
    # Unknown action
    *)
        echo "CI action \"$ACTION\" unknown."
        echo "See .ci/action.sh for available actions"
        exit 1
esac
