.PHONY: gitignore

gitignore:
	curl https://www.gitignore.io/api/node,webstorm+iml > .gitignore
	echo dist/ >> .gitignore
	echo /src/**/*.js >> .gitignore
	echo /src/**/*.js.map >> .gitignore
	echo /test/**/*.js >> .gitignore
	echo /test/**/*.js.map >> .gitignore

prepare:
	npm install -D

lint:
	npm run lint

unittest:
	npm run test
