import {DEFAULT_OPTIONS} from './constants';
import currentLocale from './constants/currentLocale';
import domain from './domain';
import {
    BlockcertsCertificate,
    RecipientClaim,
    StepGroup,
    VerifiableDocument
} from './models';
import Blockchain from "./models/blockchain";
import CowcertsCertificate from "./models/cowcertsCertificate.interface";
import Options from "./models/options";
import SignatureImage from "./models/signatureImage";
import parseCertificateDefinition from './parser';
import Verifier from './verifier';

export default class Certificate implements BlockcertsCertificate, CowcertsCertificate {

    // Blockcerts cert-verifier-js API
    certificateImage: string;
    certificateJson: any;
    chain: Blockchain;
    description: string;
    expires?: string;
    id: string;
    isFormatValid: boolean;
    issuedOn: string;
    issuer: any;
    metadataJson: any;
    name: string;
    publicKey?: string;
    rawTransactionLink?: string;
    receipt?: any;
    recipientFullName?: string;
    recordLink?: string;
    revocationKey?: string;
    sealImage?: string;
    signature?: string;
    signatureImage: Array<SignatureImage>;
    subtitle?: string;
    transactionId?: string;
    transactionLink?: string;
    verificationSteps: Array<StepGroup>;
    version: any;

    // Cowcerts API
    public document: VerifiableDocument;
    public documents: Array<VerifiableDocument> = [];
    public isCowcerts = () => false;
    public otherChains: any[];
    public endorsements: any[];
    public endorsementsMap: {};
    public recipientClaim?: RecipientClaim;

    private _options: any;
    private _locale: string;

    constructor(
        certificateDefinition,
        options: Options = DEFAULT_OPTIONS
    ) {

        // Options
        this._setOptions(options);

        // Parse
        const {blockcerts, cowcerts} = parseCertificateDefinition(
            certificateDefinition,
            options
        );

        // Set properties
        this._setBlockcertsAPIProperties(blockcerts);
        this._setCowcertsAPIProperties(cowcerts);
    }

    /**
     * verify
     *
     * @param stepCallback
     * @returns {Promise<*>}
     */
    async verify(stepCallback = () => {
    }) {
        const results = await Promise.all(
            this.documents.map(document =>
                new Verifier(document).verify(stepCallback))
        );
        if(results.every(result => !!result)) {
            return results[0];
        }
    }

    get options(): any {
        return this._options;
    }

    set options(options: any) {
        this._options = Object.assign({}, DEFAULT_OPTIONS, options);
    }

    get locale(): string {
        return this._locale;
    }

    set locale(locale: string) {
        this._locale = domain.i18n.ensureIsSupported(
            locale === 'auto' ? domain.i18n.detectLocale() : locale
        );
        currentLocale.locale = this._locale;
    }

    _setOptions(options) {
        this.options = options;
        this.locale = this.options.locale;
    }

    _setBlockcertsAPIProperties(
        certificate: BlockcertsCertificate,
    ): void {
        this.certificateJson = JSON.parse(JSON.stringify(certificate.certificateJson));
        this.certificateImage = certificate.certificateImage;
        this.chain = certificate.chain;
        this.description = certificate.description;
        this.expires = certificate.expires;
        this.id = certificate.id;
        this.issuedOn = certificate.issuedOn;
        this.issuer = certificate.issuer;
        this.metadataJson = certificate.metadataJson;
        this.name = certificate.name;
        this.publicKey = certificate.publicKey;
        this.rawTransactionLink = certificate.rawTransactionLink;
        this.receipt = certificate.receipt;
        this.recipientFullName = certificate.recipientFullName;
        this.recordLink = certificate.recordLink;
        this.revocationKey = certificate.revocationKey;
        this.sealImage = certificate.sealImage;
        this.signature = certificate.signature;
        this.signatureImage = certificate.signatureImage;
        this.subtitle = certificate.subtitle;
        this.transactionId = certificate.transactionId;
        this.transactionLink = certificate.transactionLink;
        this.verificationSteps = certificate.verificationSteps;
        this.version = certificate.version;
    }

    _setCowcertsAPIProperties(certificate: CowcertsCertificate): void {
        this.document = certificate.document;
        this.documents = certificate.documents;
        this.endorsements = certificate.endorsements;
        this.endorsementsMap = certificate.endorsementsMap;
        this.isCowcerts = certificate.isCowcerts;
        this.recipientClaim = certificate.recipientClaim;
    }
}
