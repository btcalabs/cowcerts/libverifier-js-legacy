import * as _ from 'lodash';
import i18n from '../data/i18n.json';
import {getStepGroupCodes} from "../enums";
import {StepGroup} from '../models';

function createStepGroupFromCode(stepGroupCode: string): StepGroup {
    // @ts-ignore
    return new StepGroup(
        // @ts-ignore
        stepGroupCode,
        _.get(
            i18n,
            `en-US.steps.${stepGroupCode}Label`,
            "[missing locale item data]"),
        _.get(
            i18n,
            `en-US.steps.${stepGroupCode}LabelPending`,
            "[missing locale item data]"),
    );
}

/**
 * Kept for retrocompatiblity with Blockcerts cert-verifier-js
 *
 * https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/verificationSteps.js
 */
const STEPS = {
    language: getStepGroupCodes()
        .map(stepGroupCode => createStepGroupFromCode(stepGroupCode))
        .reduce(
            (obj, stepGroup) => {
                return {...obj, [stepGroup.code]: stepGroup}
            },
            {}
        ),
    ...getStepGroupCodes()
        .reduce(
            (obj, code) => {
                return {...obj, [code]: code}
            },
            {})
};

export default STEPS;
