import {Blockchain, BlockchainTransactionTemplates} from '../models';
import BlockchainCode from '../enums/blockchainCode.enum';
import BlockchainSignatureValue from '../enums/blockchainSignatureValue.enum';

export const TRANSACTION_TEMPLATE_ID_PLACEHOLDER = '{TRANSACTION_ID}';

export const BitcoinMainnet = new Blockchain(
    BlockchainCode.BitcoinMainnet,
    'Bitcoin',
    ['6a20', 'OP_RETURN '],
    BlockchainSignatureValue.BitcoinMainnet,
    new BlockchainTransactionTemplates(
        `https://blockchain.info/tx/${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
        `https://blockchain.info/rawtx/${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
    ),
);

export const BitcoinRegtest = new Blockchain(
    BlockchainCode.BitcoinRegtest,
    'Mocknet',
    [],
    BlockchainSignatureValue.BitcoinRegtest,
    new BlockchainTransactionTemplates(
        '',
        '',
    ),
    true
);

export const BitcoinTestnet = new Blockchain(
    BlockchainCode.BitcoinTestnet,
    'Mocknet',
    [],
    BlockchainSignatureValue.bitconTestnet,
    new BlockchainTransactionTemplates(
        `https://testnet.blockchain.info/tx/${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
        `https://testnet.blockchain.info/rawtx/${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
    ),
);

export const EthereumMainnet = new Blockchain(
    BlockchainCode.EthereumMainnet,
    'Ethereum',
    ['0x'],
    BlockchainSignatureValue.EthereumMainnet,
    new BlockchainTransactionTemplates(
        `https://etherscan.io/tx/${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
        `https://etherscan.io/tx/${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
    ),
);

export const EthereumRopsten = new Blockchain(
    BlockchainCode.EthereumRopsten,
    'Ethereum',
    [],
    BlockchainSignatureValue.EthereumRopsten,
    new BlockchainTransactionTemplates(
        `https://ropsten.etherscan.io/tx/${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
        `https://ropsten.etherscan.io/getRawTx?tx=${TRANSACTION_TEMPLATE_ID_PLACEHOLDER}`,
    ),
);

export const Mocknet = new Blockchain(
    BlockchainCode.Mocknet,
    'Mocknet',
    [],
    BlockchainSignatureValue.Mockchain,
    new BlockchainTransactionTemplates(
        '',
        '',
    ),
    true
);

export const BlockchainsList = [
    BitcoinMainnet,
    BitcoinTestnet,
    BitcoinRegtest,
    EthereumMainnet,
    EthereumRopsten,
    Mocknet
];

/**
 * Map of every Blockchain object given its code value
 *
 * Keeped here for backwards-compatibility with cert-verifier-js
 */
export const BLOCKCHAINS: { [code: string]: Blockchain } =
    BlockchainsList.reduce(
        (blockchainMapByCode, blockchain) => {
            blockchainMapByCode[blockchain.code] = blockchain;
            return blockchainMapByCode;
        },
        {}
    );
