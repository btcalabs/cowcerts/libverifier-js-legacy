export default {
  ethlocal: {
    name: 'Local dummy blockchain',
    protocol: 'ETH',
    id: 'urn:example:local'
  },
  ethenclar: {
    name: 'BlockValley\'s enclar blockchain',
    protocol: 'ETH',
    id: 'https://blockvalley.info/chains/eth/enclar'
  }
};
