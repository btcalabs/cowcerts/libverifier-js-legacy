import {Options} from "../models";

const DEFAULT_OPTIONS: Options = {
  locale: 'en-US',
  legacySteps: true
};

export { DEFAULT_OPTIONS };
