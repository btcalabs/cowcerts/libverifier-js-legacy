import * as _ from 'lodash';
import i18n from '../data/i18n.json';
import {
    getStepGroupCode,
    getSubStepCode,
    getSubStepCodes,
    StepGroupEnum,
    SubStepEnum
} from '../enums';
import {SubStep} from '../models';


/**
 * Creates a substep given its enum item
 */
function createSubStep(
    subStep: SubStepEnum,
    parentStep?: string,
): SubStep {
    return createSubStepFromCode(getSubStepCode(subStep),  parentStep)
}

function createSubStepFromCode(
    subStepCode: string,
    parentStep?: string,
): SubStep {
    return new SubStep(
        // @ts-ignore
        subStepCode,
        _.get(
            i18n,
            `en-US.subSteps.${subStepCode}Label`,
            "[missing locale item data]"),
        _.get(
            i18n,
            `en-US.subSteps.${subStepCode}LabelPending`,
            "[missing locale item data]"),
        parentStep
    );
}

interface StepGroupMap {
    group: StepGroupEnum,
    steps: Array<SubStepEnum>
}

const SubStepsMap: Array<StepGroupMap> = [
    {
        group: StepGroupEnum.EndorsementChecks,
        steps: [
            SubStepEnum.EndorsementIsForDocument,
        ]
    },
    {
        group: StepGroupEnum.ContentValidation,
        steps: [
            // SubStepEnum.FormatValidation,
            SubStepEnum.ComputeHash,
            SubStepEnum.CompareHashWithSignature,
            SubStepEnum.ComputeMerkleRoot,
            SubStepEnum.CompareMerkleRootWithSignature,
        ],
    },
    {
        group: StepGroupEnum.BlockchainRegistration,
        steps: [
            SubStepEnum.FetchBlockchainMerkleRoot,
            SubStepEnum.CompareMerkleRootWithBlockchain,
        ],
    },
    {
        group: StepGroupEnum.AuthenticityChecks,
        steps: [
            SubStepEnum.FetchIssuerProfile,
            SubStepEnum.CheckIssuerKeysWereValid,
        ],
    },
    {
        group: StepGroupEnum.RevocationChecks,
        steps: [
            SubStepEnum.FetchIssuerRevocationList,
            SubStepEnum.CheckIfRevoked,
        ],
    },
    {
        group: StepGroupEnum.ExpirationChecks,
        steps: [
            SubStepEnum.CheckIfExpired,
        ],
    },
];

/**
 * Given a step group enum, return all sub step enums using the map
 */
function getSubStepEnumsFromStepGroupEnum(
    stepGroupEnum: StepGroupEnum
): Array<SubStepEnum> {
    return SubStepsMap
        .find(subStepMap => subStepMap.group === stepGroupEnum)
        .steps
}

/**
 * Kept for retrocompatiblity with Blockcerts cert-verifier-js
 *
 * https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/verificationSubSteps.js
 */
const SUB_STEPS: any = {
    language: Object.values(SubStepsMap)
        .map((stepGroupMap) => stepGroupMap.steps
            .map(subStep => createSubStep(
                subStep,
                getStepGroupCode(stepGroupMap.group))
            )
        )
        .flat()
        .reduce(
            (obj, subStep) => {
                return {...obj, [subStep.code]: subStep}
            },
            {}
        ),
    ...getSubStepCodes()
        .reduce((obj, code) => {
            return {...obj, [code]: code}
        }, {})
};

export {
    createSubStep,
    getSubStepEnumsFromStepGroupEnum,
    SubStepsMap,
    SUB_STEPS,
};
