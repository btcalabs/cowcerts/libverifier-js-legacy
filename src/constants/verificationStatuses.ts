enum VerificationStatus {
    Failure = 'failure',
    Starting = 'starting',
    Success = 'success',
}

/**
 * Kept for retrocompatibility with Blockcerts cert-verifier-js
 */
enum VERIFICATION_STATUSES {
    FAILURE = VerificationStatus.Failure,
    STARTING = VerificationStatus.Starting,
    SUCCESS = VerificationStatus.Success
}

export {
    VerificationStatus,
    VERIFICATION_STATUSES
};
