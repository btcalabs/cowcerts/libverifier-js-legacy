export default {
  BLOCKCERTS_V1: 'https://w3id.org/blockcerts/v1',
  BLOCKCERTS_V2: 'https://w3id.org/blockcerts/v2',
  COWCERTS_V0: 'https://w3id.org/cowcerts/edu/v0'
};
