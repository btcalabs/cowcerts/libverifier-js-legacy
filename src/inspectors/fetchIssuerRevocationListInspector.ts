import {request} from '../services';
import {getText} from '../domain/i18n/useCases';
import {Inspector, VerifiableDocument} from "../models";

export default class FetchIssuerRevocationListInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    async run(data?: any): Promise<any> {

        const revocationListUrl = this.doc.issuer.revocationList;

        if (!revocationListUrl) {
            return Promise.resolve([]);
        }

        const response = await request({url: revocationListUrl})
            .catch(() => {
                throw new Error(getText('errors', 'getRevokedAssertions'));
            });

        // @ts-ignore
        let issuerRevocationJson = JSON.parse(response);
        return issuerRevocationJson.revokedAssertions
            ? issuerRevocationJson.revokedAssertions
            : [];
    }
}
