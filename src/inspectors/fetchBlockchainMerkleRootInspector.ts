import {BLOCKCHAINS, CONFIG, OTHER_BLOCKCHAINS} from '../constants';
import {getText} from '../domain/i18n/useCases';
import {DocumentVersion} from "../enums";
import {
    BitcoinExplorers,
    BlockchainExplorersWithSpentOutputInfo,
    EthereumExplorers,
    EthereumWeb3Explorers
} from '../explorers';
import {Inspector, VerifiableDocument} from "../models";

export default class FetchBlockchainMerkleRootInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument
    ) {
        super(doc);
    }

    run(data?: any): Promise<any> {
        const anchor = this.doc.signature.anchors[0];
        let chain: any = anchor.chain;
        let chainId: string = chain.code;
        let protocol = 'ETH';
        if (chain.isMock && anchor.otherChains.length > 0) {
            chain = anchor.otherChains[0];
            chainId = chain.id;
        }
        const transactionId = anchor.sourceId;
        let BlockchainExplorers;
        switch (chainId) {
            case BLOCKCHAINS.bitcoin.code:
            case BLOCKCHAINS.regtest.code:
            case BLOCKCHAINS.testnet.code:
            case BLOCKCHAINS.mocknet.code:
                BlockchainExplorers = BitcoinExplorers;
                break;
            case BLOCKCHAINS.ethmain.code:
            case BLOCKCHAINS.ethropst.code:
                BlockchainExplorers = EthereumExplorers;
                break;
            case OTHER_BLOCKCHAINS.ethlocal.id:
            case OTHER_BLOCKCHAINS.ethenclar.id:
                if (protocol === 'ETH') {
                    BlockchainExplorers = EthereumWeb3Explorers;
                } else {
                    throw new Error(
                        getText('errors', 'lookForTxInvalidChain'));
                }
                break;
            default:
                throw new Error(
                    getText('errors', 'lookForTxInvalidChain')
                );
        }

        // First ensure we can satisfy the MinimumBlockchainExplorers setting
        if (CONFIG.MinimumBlockchainExplorers < 0 || CONFIG.MinimumBlockchainExplorers > BlockchainExplorers.length) {
            throw new Error(getText('errors', 'lookForTxInvalidAppConfig'));
        }
        if (CONFIG.MinimumBlockchainExplorers > BlockchainExplorersWithSpentOutputInfo.length &&
            (this.doc.version === DocumentVersion.BlockcertsV1)
        ) {
            throw new Error(getText('errors', 'lookForTxInvalidAppConfig'));
        }

        // Queue up blockchain explorer APIs
        let promises = [];
        let limit;
        if (this.doc.version === DocumentVersion.BlockcertsV1) {
            limit = CONFIG.Race ? BlockchainExplorersWithSpentOutputInfo.length : CONFIG.MinimumBlockchainExplorers;
            for (let i = 0; i < limit; i++) {
                promises.push(BlockchainExplorersWithSpentOutputInfo[i](transactionId, chainId));
            }
        } else {
            limit = CONFIG.Race ? BlockchainExplorers.length : CONFIG.MinimumBlockchainExplorers;
            for (let j = 0; j < limit; j++) {
                promises.push(BlockchainExplorers[j](transactionId, chainId));
            }
        }

        let promise = promises[0];
        return promise;
    }
}