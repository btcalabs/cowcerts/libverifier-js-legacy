import {getText} from '../domain/i18n/useCases';
import {Inspector, VerifiableDocument} from "../models";
import request from "../services/request";

export default class FetchIssuerProfileInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    run(data?: any): Promise<any> {
        const errorMessage = getText('errors', 'getIssuerProfile');
        if (!this.doc.issuer.id) {
            throw new Error(errorMessage);
        }

        return request({url: this.doc.issuer.id})
            // @ts-ignore
            .then(response => JSON.parse(response))
            .catch(() => {
                throw new Error(errorMessage);
            });

    }
}