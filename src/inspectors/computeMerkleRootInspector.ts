import sha256 from 'sha256';
import {getText} from '../domain/i18n/useCases';
import {toByteArray} from '../helpers/data';
import {Inspector} from '../models';
import VerifiableDocument from "../models/verifiableDocument";

export default class ComputeMerkleRootInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    async run(): Promise<any> {

        const receipt = this.doc.signature;
        let proofHash = receipt.targetHash;
        try {
            const proof = receipt.proof;
            const isProof = !!proof;
            if (isProof) {
                for (let index in proof) {
                    const node = proof[index];
                    let appendedBuffer;
                    if (typeof node.left !== 'undefined') {
                        appendedBuffer = toByteArray(`${node.left}${proofHash}`);
                        proofHash = sha256(appendedBuffer);
                    } else if (typeof node.right !== 'undefined') {
                        appendedBuffer = toByteArray(`${proofHash}${node.right}`);
                        proofHash = sha256(appendedBuffer);
                    } else {
                        throw new Error('We should never get here.');
                    }
                }
            }
        } catch (e) {
            throw new Error(
                getText('errors', 'unableToComputeMerkleRoot')
            );
        }

        return proofHash;
    }
}
