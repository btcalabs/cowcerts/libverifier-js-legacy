import {getText} from '../domain/i18n/useCases';
import {dateToUnixTimestamp} from '../helpers/date';
import {Inspector, VerifiableDocument} from '../models';

export default class CheckIfExpiredInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    async run(data?: any): Promise<void> {
        if (!this.doc.expires) {
            return;
        }

        const expiryDate = dateToUnixTimestamp(this.doc.expires);

        if (new Date() >= expiryDate) {
            throw new Error(
                getText('errors', 'checkIfExpired')
            );
        }
    }
}
