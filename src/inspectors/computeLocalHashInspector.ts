import jsonld from 'jsonld';
import sha256 from 'sha256';
import {CONTEXTS as ContextsMap} from '../constants';
import CONFIG from '../constants/config';
import {getText} from '../domain/i18n/useCases';
import {DocumentVersion} from "../enums";
import {toUTF8Data} from '../helpers/data';

import {Inspector, SubStep, VerifiableDocument} from '../models';

const {
    obi: OBI_CONTEXT,
    blockcerts: BLOCKCERTS_CONTEXT,
    blockcertsv1_2: BLOCKCERTSV1_2_CONTEXT,
    blockcertsv2: BLOCKCERTSV2_CONTEXT
} = ContextsMap;

const CONTEXTS = {};
// Preload contexts
CONTEXTS['https://w3id.org/blockcerts/schema/2.0-alpha/context.json'] = BLOCKCERTS_CONTEXT;
CONTEXTS['https://www.blockcerts.org/schema/2.0-alpha/context.json'] = BLOCKCERTS_CONTEXT;
CONTEXTS['https://w3id.org/openbadges/v2'] = OBI_CONTEXT;
CONTEXTS['https://openbadgespec.org/v2/context.json'] = OBI_CONTEXT;
CONTEXTS['https://w3id.org/blockcerts/v2'] = BLOCKCERTSV2_CONTEXT;
CONTEXTS['https://www.w3id.org/blockcerts/schema/2.0/context.json'] = BLOCKCERTSV2_CONTEXT;
CONTEXTS['https://w3id.org/blockcerts/v1'] = BLOCKCERTSV1_2_CONTEXT;

export default class ComputeLocalHashInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    static readonly NormalizeArguments = {
        algorithm: 'URDNA2015',
        format: 'application/nquads'
    };


    static getUnmappedFields(normalized: string): Array<string> {
        const myRegexp = /<http:\/\/fallback\.org\/(.*)>/;
        const matches = myRegexp.exec(normalized);
        if (matches) {
            const unmappedFields = [];
            for (let i = 0; i < matches.length; i++) {
                unmappedFields.push(matches[i]);
            }
            return unmappedFields;
        }
        return null;
    }

    normalizeArguments(document: any, version: DocumentVersion): any {
        let expandContext = document['@context'];
        if (version !== DocumentVersion.BlockcertsV1 && CONFIG.CheckForUnmappedFields) {
            if (expandContext.find(x => x === Object(x) && '@vocab' in x)) {
                expandContext = null;
            } else {
                expandContext.push({'@vocab': 'http://fallback.org/'});
            }
        }
        let normalizeArgs = {...ComputeLocalHashInspector.NormalizeArguments};
        if (expandContext) {
            // @ts-ignore
            normalizeArgs.expandContext = expandContext;
        }
        return normalizeArgs;
    }

    static prepareJSONLD(): void {
        const nodeDocumentLoader = jsonld.documentLoaders.node();
        const customLoader = function (url, callback) {
            if (url in CONTEXTS) {
                return callback(null, {
                    contextUrl: null,
                    document: CONTEXTS[url],
                    documentUrl: url
                });
            }
            return nodeDocumentLoader(url, callback);
        };
        jsonld.documentLoader = customLoader;
    }

    static getDocument(verifiableDocument: VerifiableDocument): any {
        const doc = JSON.parse(JSON.stringify(verifiableDocument.raw));
        delete doc['signature'];
        return doc;
    }

    run(data?: any): Promise<any> {
        const rawDoc = ComputeLocalHashInspector.getDocument(this.doc);
        const args = this.normalizeArguments(rawDoc, this.doc.version);
        ComputeLocalHashInspector.prepareJSONLD();
        return new Promise((resolve, reject) => {
            jsonld.normalize(rawDoc, args, (err, normalized) => {
                const isErr = !!err;
                if (isErr) {
                    reject(
                        getText(
                            'errors',
                            'failedJsonLdNormalization')
                    );
                } else {
                    let unmappedFields = ComputeLocalHashInspector.getUnmappedFields(normalized);
                    if (unmappedFields) {
                        reject(
                            getText('errors', 'foundUnmappedFields')
                        );
                    } else {
                        let hash = sha256(toUTF8Data(normalized));
                        resolve(hash);
                    }
                }
            });
        });
    }
}
