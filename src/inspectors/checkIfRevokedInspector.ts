import domain from '../domain';
import {SubStepEnum} from "../enums";
import {intersect} from '../helpers/array';
import {Inspector, VerifiableDocument} from '../models';

export default class CheckIfRevokedInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }
    async run(data?: any): Promise<void> {

        const revokedAddresses = data[SubStepEnum.FetchIssuerRevocationList];
        let keys = [this.doc.id];

        if (!revokedAddresses || !keys) {
            // nothing to do
            return;
        }

        keys = keys.filter(key => key != null);

        const matches = intersect(keys, revokedAddresses.map(assertion => assertion.id));

        if (matches.length > 0) {
            const indexOfMatch = revokedAddresses.findIndex(address => address.id === matches[0]);

            if (indexOfMatch > -1) {
                throw new Error(
                    domain.certificates.generateRevocationReason(
                        revokedAddresses[indexOfMatch].revocationReason
                    )
                );
            }
        }
    }
}
