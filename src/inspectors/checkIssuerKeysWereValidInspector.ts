import {getText} from '../domain/i18n/useCases';
import {SubStepEnum} from "../enums";
import {dateToUnixTimestamp} from '../helpers/date';
import {Inspector, Key, VerifiableDocument} from '../models';

export default class CheckIssuerKeysWereValidInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    static createKeyObject(rawKeyObject, finalPublicKey = null) {
        const created = rawKeyObject.created ? dateToUnixTimestamp(rawKeyObject.created) : null;
        const revoked = rawKeyObject.revoked ? dateToUnixTimestamp(rawKeyObject.revoked) : null;
        const expires = rawKeyObject.expires ? dateToUnixTimestamp(rawKeyObject.expires) : null;
        // backcompat for v2 alpha
        let publicKey = finalPublicKey;
        if (!finalPublicKey) {
            const publicKeyTemp = rawKeyObject.id || rawKeyObject.publicKey;
            publicKey = publicKeyTemp.replace('ecdsa-koblitz-pubkey:', '');
        }
        return new Key(publicKey, created, revoked, expires);
    }

    static parseIssuerKeys(issuerProfileJson: any) {
        try {
            let keyMap = {};
            if ('@context' in issuerProfileJson) {
                // backcompat for v2 alpha
                const responseKeys = issuerProfileJson.publicKey || issuerProfileJson.publicKeys;
                for (let i = 0; i < responseKeys.length; i++) {
                    const key = CheckIssuerKeysWereValidInspector
                        .createKeyObject(responseKeys[i]);
                    keyMap[key.publicKey] = key;
                }
            } else {
                // This is a v2 certificate with a v1 issuer
                const issuerKeys = issuerProfileJson.issuerKeys || [];
                const key = CheckIssuerKeysWereValidInspector
                    .createKeyObject({}, issuerKeys[0].key);
                keyMap[key.publicKey] = key;
            }
            return keyMap;
        } catch (e) {
            throw new Error(getText('errors', 'parseIssuerKeys'));
        }
    }

    static getCaseInsensitiveKey(obj, value) {
        let key = null;
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if (prop.toLowerCase() === value.toLowerCase()) {
                    key = prop;
                }
            }
        }
        return obj[key];
    }

    async run(data ?: any): Promise<void> {
        const issuerProfile = data[SubStepEnum.FetchIssuerProfile];
        const txData = data[SubStepEnum.FetchBlockchainMerkleRoot];
        const keyMap = CheckIssuerKeysWereValidInspector
            .parseIssuerKeys(issuerProfile);
        let txTime = txData.time;
        const txIssuingAddress = txData.issuingAddress;
        let validKey = false;
        const theKey =
            CheckIssuerKeysWereValidInspector.getCaseInsensitiveKey(
                keyMap,
                txIssuingAddress
            );
        txTime = dateToUnixTimestamp(txTime);
        if (theKey) {
            validKey = true;
            if (theKey.created) {
                validKey = validKey && txTime >= theKey.created;
            }
            if (theKey.revoked) {
                validKey = validKey && txTime <= theKey.revoked;
            }
            if (theKey.expires) {
                validKey = validKey && txTime <= theKey.expires;
            }
        }
        if (!validKey) {
            throw new Error(
                getText('errors', 'IssuingKeyIsNotValid')
            );
        }
    }
}
