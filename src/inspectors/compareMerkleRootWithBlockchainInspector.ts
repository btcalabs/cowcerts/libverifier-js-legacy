import {getText} from '../domain/i18n/useCases';
import {SubStepEnum} from "../enums";
import {Inspector, VerifiableDocument} from "../models";

export default class CompareMerkleRootWithBlockchainInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    async run(data?: any): Promise<void> {
        if (data[SubStepEnum.FetchBlockchainMerkleRoot].remoteHash !== this.doc.signature.merkleRoot) {
            throw new Error(
                getText('errors', 'blockchainMerkleRootDoesNotMatchDocuments')
            );
        }
    }
}
