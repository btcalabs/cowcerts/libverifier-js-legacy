import * as _ from 'lodash';
import {Inspector, VerifiableDocument} from '../models';

export default class EndorsementIsForDocumentInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    async run(data?: any): Promise<any> {

        // Is an endorsement
        if (!this.doc.isEndorsement())
            throw new Error('Document is not an endorsement.');

        // Has parent document
        if (!this.doc.parentDocument)
            throw new Error('No parent document specified.');

        // Has claim id
        const claimedDocumentId = _.get('.claim.id', this.doc.raw, null);
        if (!claimedDocumentId)
            throw new Error('Endorsement has no claim id.');

        // Claim id matches
        if(claimedDocumentId !== this.doc.parentDocument.id)
            throw new Error('Endorsement is not for the main certificate');
    }

}
