import {getText} from '../domain/i18n/useCases';
import {SubStepEnum} from "../enums";
import {Inspector,VerifiableDocument} from "../models";

export default class CompareHashWithSignatureInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    async run(data?: any): Promise<void> {
        if (data[SubStepEnum.ComputeHash] !== this.doc.signature.targetHash) {
            throw Error(
                getText('errors', 'compareHashWithSignature')
            );
        }
    }
}
