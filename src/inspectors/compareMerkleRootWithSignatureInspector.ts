import {getText} from '../domain/i18n/useCases';
import {SubStepEnum} from "../enums";
import {Inspector, VerifiableDocument} from "../models";

export default class CompareMerkleRootWithSignatureInspector extends Inspector {

    constructor(
        public doc: VerifiableDocument,
    ) {
        super(doc);
    }

    async run(data?: any): Promise<void> {
        if (data[SubStepEnum.ComputeMerkleRoot] !== this.doc.signature.merkleRoot) {
            throw new Error(
                getText('errors', 'computedMerkleRootDoesNotMatchDocuments')
            );
        }
    }
}
