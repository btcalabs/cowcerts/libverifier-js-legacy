import {BlockcertsCertificate} from '../models';

export default function ensureFormatIsValid(certificate: BlockcertsCertificate) {
    // Chain is valid
    if(!certificate.chain)
        throw Error('No valid chain found');
}
