import CheckIfExpiredInspector from './checkIfExpiredInspector';
import CheckIfRevokedInspector from './checkIfRevokedInspector';
import CheckIssuerKeysWereValidInspector
    from './checkIssuerKeysWereValidInspector';
import CompareHashWithSignatureInspector
    from './compareHashWithSignatureInspector';
import CompareMerkleRootWithBlockchainInspector
    from './compareMerkleRootWithBlockchainInspector';
import CompareMerkleRootWithSignatureInspector
    from './compareMerkleRootWithSignatureInspector';
import ComputeLocalHashInspector from './computeLocalHashInspector';
import ComputeMerkleRootInspector from './computeMerkleRootInspector';
import EndorsementIsForDocumentInspector
    from './endorsementIsForDocumentInspector';
import FetchBlockchainMerkleRootInspector
    from './fetchBlockchainMerkleRootInspector';
import FetchIssuerProfileInspector from './fetchIssuerProfileInspector';
import FetchIssuerRevocationListInspector
    from './fetchIssuerRevocationListInspector';

export {
    EndorsementIsForDocumentInspector,
    ComputeLocalHashInspector,
    CompareHashWithSignatureInspector,
    ComputeMerkleRootInspector,
    CompareMerkleRootWithSignatureInspector,
    FetchBlockchainMerkleRootInspector,
    CompareMerkleRootWithBlockchainInspector,
    FetchIssuerProfileInspector,
    CheckIssuerKeysWereValidInspector,
    FetchIssuerRevocationListInspector,
    CheckIfRevokedInspector,
    CheckIfExpiredInspector,
};
