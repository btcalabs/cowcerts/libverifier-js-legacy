import BlockcertsCertificate from "./blockcertsCertificate";
import CowcertsCertificate from "./cowcertsCertificate.interface";

export default interface ParsingResult {
    blockcerts: BlockcertsCertificate,
    cowcerts: CowcertsCertificate
}