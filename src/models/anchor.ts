import Blockchain from "./blockchain";
import OtherChain from './otherChain';

export default class Anchor {

    constructor(
        public sourceId: string,
        public chain: Blockchain,
        public type: string,
        public otherChains: Array<OtherChain> = []
    ) {

    }
}
