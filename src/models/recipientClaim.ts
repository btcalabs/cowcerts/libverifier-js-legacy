export default class RecipientClaim {
    constructor(
        public id: string,
        public givenName: string,
        public familyName: string,
        public birthplace: string,
        public birthdate: string,
        public nationality: string,
        public nationalId: string) { }
}
