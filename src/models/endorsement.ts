export default class Endorsement {
    constructor(
        public id,
        public type,
        public claim,
        public issuedOn,
        public issuer,
        public verification,
        public signature,
        public signatureLines
    ) { }

    static fromJSON(endorsementJson: any) {
        return new Endorsement(
            endorsementJson.id,
            endorsementJson.type,
            endorsementJson.claim,
            endorsementJson.issuedOn,
            endorsementJson.issuer,
            endorsementJson.verification,
            endorsementJson.signature,
            endorsementJson.signatureLines,
        )
    }
}
