import {RecipientClaim, VerifiableDocument} from './index';

export default interface CowcertsCertificate {
    document: VerifiableDocument;
    documents: Array<VerifiableDocument>;
    endorsements: Array<any>;
    endorsementsMap: { [claim: string]: any };
    isCowcerts(): boolean
    recipientClaim?: RecipientClaim;
}