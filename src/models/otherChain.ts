export default class OtherChain {
  constructor (
      public id: string,
      public name: string,
      public protocol: string,
      public genesis: string,
      public consortium: string,
  ) { }
}
