export default class Key {
  constructor (
      public publicKey: string,
      public created,
      public revoked,
      public expires,
  ) { }
}
