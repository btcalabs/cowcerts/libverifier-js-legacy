import {SubStepEnum} from "../enums";
import Step from './step';
import StepGroup from './stepGroup';

export default class SubStep extends Step {


    /**
     * Kept for compatibility with Blockcerts' cert-verifier-js
     * so that the blockcerts-verifier can know their parent by code.
     */
    public parentStep?: string;
    private _group?: StepGroup;

    constructor(
        code: SubStepEnum,
        label: string,
        labelPending: string,
        parentStep?: string,
    ) {
        super(code, label, labelPending);
        this.parentStep = parentStep;
        delete this._group;
    }

    get group(): StepGroup {
        return this._group;
    }

    set group(stepGroup: StepGroup) {
        if(stepGroup) {
            this._group = stepGroup;
            this.parentStep = stepGroup.code;
        }
    }
}
