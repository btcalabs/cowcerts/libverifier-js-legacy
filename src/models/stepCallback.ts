import {VerificationStatus} from "../constants";
import {SubStepEnum} from "../enums";
import SubStep from "./subStep";
import verifiableDocument from "./verifiableDocument";
import VerifiableDocument from "./verifiableDocument";

export default class StepCallback extends SubStep {
    constructor(
        code: SubStepEnum,
        label: string,
        labelPending: string,
        public status: VerificationStatus,
        public errorMessage?: string,
        public data?: any,
        public document?: VerifiableDocument
    ) {
        super(code, label, labelPending);
    }

    static fromSubStep(
        step: SubStep,
        status: VerificationStatus,
        errorMessage?: string,
        data?: any,
        document?: VerifiableDocument,
    ): StepCallback {
        return new StepCallback(
            // @ts-ignore
            step.code,
            step.label,
            step.labelPending,
            status,
            errorMessage,
            data,
            verifiableDocument
        )
    }
}
