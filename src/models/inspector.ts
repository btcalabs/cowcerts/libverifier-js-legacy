import VerifiableDocument from "./verifiableDocument";

export default abstract class Inspector {

    protected constructor (
        public doc: VerifiableDocument,
    ) {

    }

    abstract run(data: any): Promise<any>
}
