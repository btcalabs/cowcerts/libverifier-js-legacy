import {StepGroupEnum} from "../enums";
import Step from './step';
import SubStep from './subStep';

export default class StepGroup extends Step {
    constructor(
        code: StepGroupEnum,
        label: string,
        labelPending: string,
        public subSteps: Array<SubStep> = [],
    ) {
        super(code, label, labelPending);
    }
};
