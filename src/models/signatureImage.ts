export default class SignatureImage {
  constructor (
      public image: string,
      public jobTitle: string,
      public name: string,
  ) { }
}
