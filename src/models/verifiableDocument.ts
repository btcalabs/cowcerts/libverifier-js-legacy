import {DocumentType, DocumentVersion} from '../enums';
import MerkleProof2019Signature from './merkleProof2019Signature';
import StepGroup from './stepGroup';

export default class VerifiableDocument {

    constructor(
        public raw: any,
        public id: string,
        public type: DocumentType,
        public version: DocumentVersion,
        public name: string,
        // TODO: Issuer model
        public issuer: any,
        public signature: MerkleProof2019Signature,
        public expires?: Date,
        public revocationKey?: string,
        public parentDocument?: VerifiableDocument,
        public steps: Array<StepGroup> = []
    ) {

    }

    isEndorsement(): boolean {
        return this.type === DocumentType.Endorsement;
    }
}
