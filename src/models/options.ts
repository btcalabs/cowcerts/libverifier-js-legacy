export default interface Options {
    locale: string,
    legacySteps: boolean
}