import Blockchain from './blockchain';
import StepGroup from "./stepGroup";
import SignatureImage from './signatureImage';

/**
 * API for the Certificate retrieved directly from the parser
 *
 * Inherited from
 * https://github.com/blockchain-certificates/cert-verifier-js#certificate
 */
export default interface BlockcertsCertificate {

    certificateImage: string,
    certificateJson: any,
    chain: Blockchain,
    description: string,
    expires?: string,
    id: string,
    isFormatValid: boolean,
    issuedOn: string,
    issuer: any,
    metadataJson: any,
    name: string,
    publicKey?: string,
    rawTransactionLink?: string,
    receipt?: any,
    recipientFullName?: string,
    recordLink?: string,
    revocationKey?: string,
    sealImage?: string,
    signature?: string,
    signatureImage: Array<SignatureImage>,
    subtitle?: string,
    transactionId?: string,
    transactionLink?: string,
    verificationSteps: Array<StepGroup>
    version: any,
}
