export default class TransactionData {
  constructor (
      public remoteHash: any,
      public issuingAddress: any,
      public time: Date,
      public revokedAddresses: any) { }
}
