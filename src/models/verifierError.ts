export default class VerifierError extends Error {
  constructor (public stepCode: string, message: string) {
    super(message);
  }
}
