import {BlockchainCode, BlockchainSignatureValue} from '../enums';
import BlockchainTransactionTemplates from './blockchainTransactionTemplates';

export default class Blockchain {
    constructor(
        public code: BlockchainCode,
        public name: string,
        public prefixes: Array<string> = [],
        public signatureValue: BlockchainSignatureValue,
        public transactionTemplates: BlockchainTransactionTemplates,
        public isMock = false
    ) {

    }
}
