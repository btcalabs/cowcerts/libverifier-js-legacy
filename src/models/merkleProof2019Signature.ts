import Anchor from './anchor';

export default class MerkleProof2019Signature {

    constructor(
        public merkleRoot: string,
        public targetHash: string,
        public anchors: Array<Anchor> = [],
        public proof: any,
        public endorsements: Array<any> = []
    ) {

    }
}
