export default class Step {
    constructor(
        public code: string,
        public label: string,
        public labelPending: string
    ) {

    }
}
