import debug from 'debug';
import {VerificationStatus} from './constants';
import domain from './domain';
import getInspectorForSubStep
    from "./domain/steps/useCases/getInspectorForSubStep";
import {DocumentVersion, StepGroupEnum} from "./enums";
import {
    StepCallback,
    SubStep,
    VerifiableDocument,
    VerifierError
} from './models';

const log = debug('Verifier');

export default class Verifier {

    constructor(
        public document: VerifiableDocument,
    ) {

    }

    /**
     * verify
     */
    async verify(
        stepCallback: (callback: StepCallback) => void
    ) {

        if (this.document.version === DocumentVersion.BlockcertsV1) {
            throw new VerifierError(
                '',
                'Verification of 1.1 certificates is not supported by this component. See the python cert-verifier for legacy verification'
            );
        }

        // Statuses

        // Loop groups and inspect
        let data = {};
        for (let stepGroup of this.document.steps) {
            for (let subStep of stepGroup.subSteps) {
                const inspector = getInspectorForSubStep(
                    // @ts-ignore
                    subStep.code,
                    this.document
                );
                try {
                    data[subStep.code] = await inspector.run(data);
                    stepCallback(
                        this.createSuccessCallback(
                            subStep,
                            data,
                        ),
                    );
                } catch (error) {
                    stepCallback(
                        this.createFailureCallback(
                            subStep,
                            error.message
                        )
                    );
                    return this._failed(error);
                }
            }
        }

        return this._succeed();
    }


    /** ;
     * _failed
     *
     * Returns a failure final step message
     *
     * @param errorMessage
     * @returns {{code: string, status: string, errorMessage: *}}
     * @private
     */
    _failed(errorMessage) {
        log(`failure:${errorMessage}`);
        return {
            code: StepGroupEnum.Final,
            status: VerificationStatus.Failure,
            errorMessage
        };
    }

    _succeed() {
        const logMessage = domain.chains.isMockChain(
            this.document.signature.anchors[0].chain, [])
            ? domain.i18n.getText('success', 'mocknet')
            : domain.i18n.getText('success', 'blockchain');
        log(logMessage);
        return {code: StepGroupEnum.Final, status: VerificationStatus.Success};
    }

    createSuccessCallback(step: SubStep, data?: any): StepCallback {
        return StepCallback.fromSubStep(
            step,
            VerificationStatus.Success,
            undefined,
            data,
            this.document
        )
    }

    createFailureCallback(step: SubStep, errorMessage?: string): StepCallback {
        return StepCallback.fromSubStep(
            step,
            VerificationStatus.Failure,
            errorMessage,
            undefined,
            this.document
        );
    }

}
