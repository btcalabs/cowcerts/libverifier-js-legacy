import {BLOCKCHAINS} from '../constants';
import {TransactionData} from '../models';
import {getText} from '../domain/i18n/useCases';
import {OTHER_BLOCKCHAINS} from '../constants';
import {stripHashPrefix} from './utils/stripHashPrefix';
import {request} from '../services';

export async function getEthereumWeb3Fetcher(transactionId, chain) {
    let web3 = getWeb3FromChain(chain);
    let tx = await getTransactionByHash(web3, transactionId);
    let block = await getBlockByHash(web3, tx.blockHash);
    return createTransactionData(tx, block);
}

class Web3Eth {
    private web3: any;

    constructor(web3) {
        this.web3 = web3;
    }

    getTransaction(transactionId): Promise<any> {
        return this.request(
            'getTransactionByHash',
            [transactionId]
        );
    }

    getBlock(blockHash): Promise<any> {
        return this.request(
            'getBlockByHash',
            [blockHash, false]
        ).then((block: any) => {
            block.timestamp = parseInt(block.timestamp, 16);
            return block;
        });
    }

    request(method, params) {
        return request({
            method: 'POST',
            url: this.web3.provider,
            contentType: 'application/json',
            body: {
                jsonrpc: '2.0',
                method: `eth_${method}`,
                params: params,
                id: 1
            }
        }).then((result: any) => {
            const parsedResult = JSON.parse(result);
            if (parsedResult.result) {
                return parsedResult.result;
            } else {
                throw new Error(parsedResult.error);
            }
        })
    }
}

class Web3 {
    private eth: Web3Eth;

    constructor(public provider) {
        this.eth = new Web3Eth(this);
    }
}

function getWeb3FromChain(chain) {
    const provider = getWeb3ProviderFromChain(chain);
    if (provider === null) {
        throw new Error(
            getText('errors', 'lookForTxInvalidChain')
        )
    }
    return new Web3(provider);
}

function getWeb3ProviderFromChain(chain) {
    if (chain === OTHER_BLOCKCHAINS.ethlocal.id) {
        return `http://${getCurrentHost()}:8545`;
    } else if (chain === OTHER_BLOCKCHAINS.ethenclar.id) {
        return `https://explorer.blockvalley.info:8547`;
    } else {
        return null;
    }
}

function getCurrentHost() {
    return window.location.hostname || 'localhost';
}

function getTransactionByHash(web3, transactionId): Promise<any> {
    return web3.eth.getTransaction(transactionId);
}

function getBlockByHash(web3, blockHash): Promise<any> {
    return web3.eth.getBlock(blockHash);
}

function createTransactionData(tx, block) {
    const opReturnScript = stripHashPrefix(tx.input, BLOCKCHAINS.ethmain.prefixes);
    const issuingAddress = tx.from;
    const date = new Date(block.timestamp * 1000);

    return new TransactionData(
        opReturnScript,
        issuingAddress,
        date,
        undefined
    );
}
