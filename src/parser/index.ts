import * as _ from 'lodash';
import domain from '../domain';
import {BlockcertsCertificate} from '../models';
import Options from "../models/options";
import ParsingResult from "../models/parsingResult.interface";
import BlockcertsCertificateBlockcertsV1Parser
  from './blockcerts/blockcerts-v1';
import BlockcertsCertificateBlockcertsV2Parser
  from './blockcerts/blockcerts-v2';
import BlockcertsCertificateCowcertsV0Parser from './blockcerts/cowcerts-v0';
import CowcertsCertificateCowcertsV0Parser from "./cowcerts/cowcerts-v0";
import ParserInterface from './parser.interface';

export function certificateDefinitionToJSON(certificateDefinition: any): any {
  if (_.isString(certificateDefinition)) {
    try {
      certificateDefinition = JSON.parse(certificateDefinition);
    } catch (err) {
      throw new Error(domain.i18n.getText('errors', 'certificateNotValid'));
    }
  } else if(!_.isObject(certificateDefinition)) {
    throw new Error(domain.i18n.getText('errors', 'certificateNotValid'));
  }

  return certificateDefinition
}

export default function parseCertificateDefinition (
    certificateDefinition: any,
    options: Options
): ParsingResult {

  const certificateJson = certificateDefinitionToJSON(certificateDefinition);

  const context = certificateJson['@context'];
  let selectedBlockcertsAPIParser: ParserInterface<BlockcertsCertificate>;

  let cowcertsAPICertificate;

  try {
      cowcertsAPICertificate =
        new CowcertsCertificateCowcertsV0Parser(options)
            .parse(certificateJson);
  } catch {
    throw Error('Invalid certificate format');
  }

  const blockcertsAPIParsers = [
      BlockcertsCertificateCowcertsV0Parser,
      BlockcertsCertificateBlockcertsV2Parser,
      BlockcertsCertificateBlockcertsV1Parser
  ];

  for(let blockcertsAPIParser of blockcertsAPIParsers) {
    selectedBlockcertsAPIParser = new blockcertsAPIParser(options);
    if(new blockcertsAPIParser().canParse(context)) {
      break;
    }
  }

  const blockertsAPICertificate =
      selectedBlockcertsAPIParser.parse(
          certificateJson,
          cowcertsAPICertificate,
      );

  return {
    blockcerts: blockertsAPICertificate,
    cowcerts: cowcertsAPICertificate
  }
}
