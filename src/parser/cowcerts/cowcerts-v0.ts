import * as _ from 'lodash';
import {DEFAULT_OPTIONS} from "../../constants";
import domain from "../../domain";
import {Endorsement, Options, RecipientClaim} from "../../models";
import CowcertsCertificate from "../../models/cowcertsCertificate.interface";
import ParserInterface from "../parser.interface";

export default class CowcertsCertificateCowcertsV0Parser
    implements ParserInterface<CowcertsCertificate> {

    constructor(
        public options: Options = DEFAULT_OPTIONS
    ) {
    }


    canParse(context: any): boolean {
        return domain.verifiableDocuments.isCowcertsV0(context);
    }

    static findEndorsements(signature) {
        if (
            signature instanceof Object &&
            signature.endorsements instanceof Array) {
            return signature.endorsements.map(
                (endorsement) => new Endorsement(
                    endorsement.id,
                    endorsement.type,
                    endorsement.claim,
                    endorsement.issuedOn,
                    endorsement.issuer,
                    endorsement.verification,
                    endorsement.signature,
                    endorsement.signatureLines
                ));
        }
        return [];
    }

    static endorsementsMapByClaimType(endorsements) {
        const endorsementsMap = {};
        for (let endorsement of endorsements) {
            // Has claim
            if (_.has(endorsement, 'claim') && _.isObject(endorsement.claim)) {
                let claim = endorsement.claim;
                if (_.has(claim, 'type')) {
                    let claimType = claim.type;
                    if (_.isArray(claimType)) {
                        // Claim types is an array
                        for (let type of claimType) {
                            if (type !== 'Extension') {
                                endorsementsMap[type] = endorsement;
                            }
                        }
                    } else if (_.isString(claimType)) {
                        // Claim type is a string
                        endorsementsMap[claimType] = endorsement;
                    }
                }
            }
        }
        return endorsementsMap;
    }

    static recipientClaim(endorsementsMap): RecipientClaim {
        if (_.has(endorsementsMap, 'RecipientClaim') && _.isObject(endorsementsMap['RecipientClaim'])) {
            const claim = endorsementsMap['RecipientClaim'].claim;
            return new RecipientClaim(
                claim.id,
                claim.givenName,
                claim.familyName,
                claim.birthplace,
                claim.birthdate,
                claim.nationality,
                claim.nationalId
            );
        } else {
            return null;
        }
    }

    parse(rawCertificate: any): CowcertsCertificate {

        const document = domain.verifiableDocuments.parseFromJson(
            rawCertificate,
            undefined,
            this.options.legacySteps
        );

        const endorsements = CowcertsCertificateCowcertsV0Parser.findEndorsements(
            rawCertificate.signature
        );

        const documents = [
            document,
            ...endorsements.map(endorsement =>
                domain.verifiableDocuments.parseFromJson(
                    endorsement,
                    document,
                    this.options.legacySteps
                )
            )
        ];

        const endorsementsMap = CowcertsCertificateCowcertsV0Parser.endorsementsMapByClaimType(
            endorsements
        );
        const recipientClaim = CowcertsCertificateCowcertsV0Parser.recipientClaim(
            endorsementsMap
        );

        return {
            document,
            documents,
            endorsements,
            endorsementsMap,
            isCowcerts: () => this.canParse(rawCertificate['@context']),
            recipientClaim,
        };
    }

}