import { CERTIFICATE_VERSIONS } from '../../constants';
import SignatureImage from '../../models/signatureImage';

/**
 * _getSignatureImages
 *
 * @param signatureRawObject
 * @param certificateVersion
 * @returns {Array}
 * @private
 */
export default function getSignatureImages (signatureRawObject, certificateVersion) {
  let signatureImageObjects = [];

  switch (certificateVersion) {
    case CERTIFICATE_VERSIONS.V1_1:
    case CERTIFICATE_VERSIONS.V1_2:
      if (signatureRawObject.constructor === Array) {
        for (let index in signatureRawObject) {
          let signatureLine = signatureRawObject[index];
          let jobTitle = 'jobTitle' in signatureLine ? signatureLine.jobTitle : null;
          let signerName = 'name' in signatureLine ? signatureLine.name : null;
          let signatureObject = new SignatureImage(signatureLine.image, jobTitle, signerName);
          signatureImageObjects.push(signatureObject);
        }
      } else {
        let signatureObject = new SignatureImage(signatureRawObject, null, null);
        signatureImageObjects.push(signatureObject);
      }
      break;

    case CERTIFICATE_VERSIONS.V2_0:
      for (let index in signatureRawObject) {
        let signatureLine = signatureRawObject[index];
        let signatureObject = new SignatureImage(signatureLine.image, signatureLine.jobTitle, signatureLine.name);
        signatureImageObjects.push(signatureObject);
      }
      break;
  }

  return signatureImageObjects;
}
