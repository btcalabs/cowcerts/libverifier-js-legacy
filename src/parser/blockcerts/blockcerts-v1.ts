import ParserInterface from '../parser.interface';
import getSignatureImages from './signatureImages';
import {CERTIFICATE_VERSIONS, DEFAULT_OPTIONS} from '../../constants';
import domain from '../../domain';
import {BlockcertsCertificate, Options} from '../../models';

export default class BlockcertsCertificateBlockcertsV1Parser
    implements ParserInterface<BlockcertsCertificate> {

    constructor(
        public options: Options = DEFAULT_OPTIONS,
    ) {

    }

    canParse(context: any): boolean {
        return domain.verifiableDocuments.isBlockcertsV1(context);
    }

    /**
     * Parses a Blockcerts v1 certificate.
     *
     * @param certificateJson
     * @returns {Document}
     */
    parse(certificateJson): BlockcertsCertificate {
        try {
            const fullCertificateObject = certificateJson.certificate || certificateJson.document.certificate;
            const recipient = certificateJson.recipient || certificateJson.document.recipient;
            const assertion = certificateJson.document.assertion;

            const receipt = certificateJson.receipt;
            const version = typeof receipt === 'undefined' ? CERTIFICATE_VERSIONS.V1_1 : CERTIFICATE_VERSIONS.V1_2;

            let {image: certificateImage, description, issuer, subtitle} = fullCertificateObject;

            const publicKey = recipient.publicKey;
            const chain = domain.certificates.getChain(publicKey);
            const expires = assertion.expires;
            const id = assertion.uid;
            const issuedOn = assertion.issuedOn;
            const metadataJson = assertion.metadataJson;
            const recipientFullName = `${recipient.givenName} ${recipient.familyName}`;
            const recordLink = assertion.id;
            const revocationKey = recipient.revocationKey || null;
            const sealImage = issuer.image;
            const signature = certificateJson.document.signature;
            const signaturesRaw = certificateJson.document && certificateJson.document.assertion && certificateJson.document.assertion['image:signature'];
            const signatureImage = getSignatureImages(signaturesRaw, version);
            if (typeof subtitle === 'object') {
                subtitle = subtitle.display ? subtitle.content : '';
            }
            let name = fullCertificateObject.title || fullCertificateObject.name;

            const transactionId = domain.certificates.getTransactionId(receipt);
            const rawTransactionLink = domain.certificates.getTransactionLink(
                transactionId, chain, true,
            );
            const transactionLink = domain.certificates.getTransactionLink(
                transactionId, chain,
            );

            return {
                certificateImage,
                certificateJson,
                chain,
                description,
                expires,
                id,
                isFormatValid: true,
                issuedOn,
                issuer,
                metadataJson,
                name,
                publicKey,
                rawTransactionLink,
                receipt,
                recipientFullName,
                recordLink,
                revocationKey,
                sealImage,
                signature,
                signatureImage,
                subtitle,
                transactionId,
                transactionLink,
                verificationSteps: domain.certificates.getVerificationMap(chain),
                version,
            };
        } catch (err) {
            throw Error(`Invalid certificate format: ${err.toString()}`)
        }
    }
}
