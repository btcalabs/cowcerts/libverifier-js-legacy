import Options from "../../models/options";
import getSignatureImages from './signatureImages';
import {CERTIFICATE_VERSIONS, DEFAULT_OPTIONS} from '../../constants';
import domain from '../../domain';
import ParserInterface from '../parser.interface';
import {BlockcertsCertificate} from '../../models';

export default class BlockcertsCertificateBlockcertsV2Parser
    implements ParserInterface<BlockcertsCertificate> {

    constructor(
        public options: Options = DEFAULT_OPTIONS,
    ) {

    }

    /**
     * Detects if the certificates is a Blockcerts v2 certificate.
     *
     * Implementation is based on cert-verifier-js:
     * https://github.com/blockchain-certificates/cert-verifier-js/blob/f819de20ae13b27790355fe39a0fbb8b92855856/src/parser.js#L157-L162
     *
     * IMPORTANT: Cowcerts checks first, as this is too generic.
     *
     * @param context
     * @returns {Boolean}
     */
    canParse(context: any): boolean {
        return domain.verifiableDocuments.isBlockcertsV2(context);
    }

    /**
     * parseV2
     *
     * @param certificateJson
     * @returns {{certificateImage: *, chain: *, expires: *, recordLink: *, signature: null, metadataJson, revocationKey: null, description: *, sealImage: *, publicKey: *, version: string, issuer: *, issuedOn: *, signatureImage: Array, subtitle: *, name: *, recipientFullName: *, receipt: *, id: *}}
     */
    parse(certificateJson): BlockcertsCertificate {
        try {
            const {id, expires, signature: receipt, badge} = certificateJson;
            const {image: certificateImage, name, description, subtitle, issuer} = badge;
            const issuerKey = certificateJson.verification.publicKey || certificateJson.verification.creator;
            const recipientProfile = certificateJson.recipientProfile || certificateJson.recipient.recipientProfile;

            const version = CERTIFICATE_VERSIONS.V2_0;
            const chain = domain.certificates.getChain(issuerKey, certificateJson.signature);
            const issuedOn = certificateJson.issuedOn;
            const metadataJson = certificateJson.metadataJson;
            const publicKey = recipientProfile.publicKey;
            const recipientFullName = recipientProfile.name;
            const recordLink = certificateJson.id;
            const revocationKey = null;
            const sealImage = issuer.image;
            const signatureImage = getSignatureImages(badge.signatureLines, version);

            const transactionId = domain.certificates.getTransactionId(receipt);
            const rawTransactionLink = domain.certificates.getTransactionLink(
                transactionId, chain, true,
            );
            const transactionLink = domain.certificates.getTransactionLink(
                transactionId, chain,
            );


            return {
                certificateImage,
                certificateJson,
                chain,
                description,
                expires,
                id,
                isFormatValid: true,
                issuedOn,
                issuer,
                metadataJson,
                name,
                publicKey,
                rawTransactionLink,
                receipt,
                recipientFullName,
                recordLink,
                revocationKey,
                sealImage,
                signature: null,
                signatureImage,
                subtitle,
                transactionId,
                transactionLink,
                verificationSteps: domain.certificates.getVerificationMap(chain),
                version,
            };
        } catch (err) {
            throw Error(`Invalid certificate format: ${err.toString()}`)
        }
    }
}
