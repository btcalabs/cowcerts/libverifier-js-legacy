import BlockcertsCertificateBlockcertsV1Parser from "./blockcerts-v1";
import BlockcertsCertificateBlockcertsV2Parser from "./blockcerts-v2";
import BlockcertsCertificateCowcertsV0Parser from "./cowcerts-v0";

export default {
    BlockcertsV1Parser: BlockcertsCertificateBlockcertsV1Parser,
    BlockcertsV2Parser: BlockcertsCertificateBlockcertsV2Parser,
    CowcertsV0Parser: BlockcertsCertificateCowcertsV0Parser
}