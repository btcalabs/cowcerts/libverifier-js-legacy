import * as _ from 'lodash';
import {CERTIFICATE_VERSIONS, DEFAULT_OPTIONS} from '../../constants';
import domain from '../../domain';
import {BlockcertsCertificate, Options} from '../../models';
import CowcertsCertificate from "../../models/cowcertsCertificate.interface";
import ParserInterface from '../parser.interface';
import getSignatureImages from './signatureImages';
import BlockcertsCertificateBlockcertsV2Parser from './blockcerts-v2';

export default class BlockcertsCertificateCowcertsV0Parser
    implements ParserInterface<BlockcertsCertificate> {

    constructor(
        public options: Options = DEFAULT_OPTIONS,
        protected _blockcertsParser = new BlockcertsCertificateBlockcertsV2Parser()
    ) {

    }

    /**
     * Detects if the certificates is a Cowcerts V0 certificate.
     *
     * @param context
     * @returns {Boolean}
     */
    canParse(context: any): boolean {
        return domain.verifiableDocuments.isCowcertsV0(context);
    }

    parse(
        certificateJson,
        cowcertsCertificate?: CowcertsCertificate
    ):
        BlockcertsCertificate {
        const certificate = this._blockcertsParser.parse(certificateJson);

        // Read signatureLines on certificate
        certificate.signatureImage = certificate.signatureImage || [];
        certificate.signatureImage = certificate.signatureImage.concat(
            getSignatureImages(
                certificateJson.signatureLines,
                CERTIFICATE_VERSIONS.V2_0
            ));

        // Replace steps with calculated document steps
        certificate.verificationSteps = cowcertsCertificate.document.steps;

        // Replace fullname using recipient claim
        if (cowcertsCertificate && cowcertsCertificate.recipientClaim) {
            const recipientClaim = cowcertsCertificate.recipientClaim;
            if (_.has(recipientClaim, 'givenName') && _.isString(recipientClaim.givenName) &&
                _.has(recipientClaim, 'familyName') && _.isString(recipientClaim.familyName)) {
                certificate.recipientFullName = `${recipientClaim.givenName} ${recipientClaim.familyName}`;
            }
        }

        return certificate;
    }
}
