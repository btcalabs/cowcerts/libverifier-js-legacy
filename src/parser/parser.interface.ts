import {Options} from "../models";
import CowcertsCertificate from '../models/cowcertsCertificate.interface';

export default interface ParserInterface<T> {

  options?: Options

  canParse (context: any): boolean
  parse (
      certificate: any,
      cowcertsCertificate?: CowcertsCertificate
  ): T
}
