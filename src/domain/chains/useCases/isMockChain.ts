import {BLOCKCHAINS} from "../../../constants";
import {Blockchain, OtherChain} from '../../../models';
import * as _ from 'lodash';

export default function isMockChain(
    chain: Blockchain | string,
    otherChains: Array<OtherChain>
): boolean {

    let isMock;
    // Blockcerts consideration for being mock
    //
    // https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/domain/chains/useCases/isMockChain.js
    // https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/blockchains.js
    //
    // This changed recently:
    // https://github.com/blockchain-certificates/cert-verifier-js/commits/master/src/constants/blockchains.js
    //
    let chainObject: Blockchain;
    if(_.isString(chain)) {
        // TypeScript
        chain = chain.toString();
        chainObject = BLOCKCHAINS[chain];
        if(!chainObject)
            return null;
    } else if (chain instanceof Blockchain) {
        chainObject = chain;
    } else if(chain === null || chain === undefined) {
        return null;
    }
    const isBlockcertsMock = !!chainObject.isMock;

    // Cowcerts considerations for being mock: no chains
    // OtherChains evaluation to keep compatibility with Blockcerts (just one
    // argument)
    if(otherChains) {
        const isCowcertsMock = otherChains.length === 0;
        isMock = isBlockcertsMock && isCowcertsMock;
    } else {
        isMock = isBlockcertsMock;
    }

    // Convert to null for backwards-compatibility
    return isMock;
}
