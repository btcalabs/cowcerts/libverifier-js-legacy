import * as _ from 'lodash';

/**
 * Detects if the certificates is a Blockcerts v2 certificate.
 *
 * Implementation is based on cert-verifier-js:
 * https://github.com/blockchain-certificates/cert-verifier-js/blob/f819de20ae13b27790355fe39a0fbb8b92855856/src/parser.js#L157-L162
 *
 * IMPORTANT: Cowcerts checks first, as this is too generic.
 *
 * @param context
 * @returns {Boolean}
 */
export default function isBlockcertsV2 (context: any): boolean {
    return _.isArray(context);
}
