import {DocumentType,DocumentVersion} from '../../../enums';

export default function getName(
    raw: any,
    type: DocumentType,
    version: DocumentVersion
): string {
    if (type === DocumentType.Certificate) {
        if(version == DocumentVersion.BlockcertsV1) {
            const fullCertificateObject = raw.certificate || raw.document.certificate;
            return fullCertificateObject.title || fullCertificateObject.name;
        } else {
            return raw.badge.name;
        }
    } else {
        let claimTypes = raw.claim.type;
        let claimType = claimTypes.filter(claim => claim !== 'Extension')[0];
        let claim = claimType.replace(new RegExp('Claim$'), '');
        return claim;
    }
}
