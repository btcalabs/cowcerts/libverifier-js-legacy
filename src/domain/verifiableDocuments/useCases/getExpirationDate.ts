export default function getExpirationDate(raw: any): Date {
    if(raw.expires) {
        return new Date(raw.expires)
    }
    return null;
}
