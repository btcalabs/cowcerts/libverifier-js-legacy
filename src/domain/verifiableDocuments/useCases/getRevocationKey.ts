import * as _ from 'lodash';


export default function getRevocationKey(raw: any): string {
    return _.get(raw, 'recipient.revocationKey', null)
        || _.get(raw, 'document.recipient.revocationKey', null)
}
