import * as _ from 'lodash';
import {DocumentType,DocumentVersion} from '../../../enums';

export default function getIssuer(
    raw: any,
    type: DocumentType,
    version: DocumentVersion
): any {

    let issuer = null;

    // Detect and get
    if (type === DocumentType.Certificate) {
        if (version === DocumentVersion.BlockcertsV1) {
            const fullCertificateObject = raw.certificate || raw.document.certificate;
            return fullCertificateObject.issuer;
        } else {
            issuer = _.get(raw, 'badge.issuer', null);
        }
    } else if (type === DocumentType.Endorsement) {
        issuer = _.get(raw, 'issuer', null);
    }

    // No issuer
    if(issuer === null) {
        throw new Error('Issuer not found');
    }

    return issuer

}
