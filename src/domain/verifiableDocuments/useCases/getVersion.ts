import {DocumentType, DocumentVersion} from '../../../enums';
import VerifiableDocument from "../../../models/verifiableDocument";
import isBlockcertsV1 from "./isBlockcertsV1";
import isBlockcertsV2 from './isBlockcertsV2';
import isCowcertsV0 from "./isCowcertsV0";

export default function getVersion(
    raw: any,
    type?: DocumentType,
    parent?: VerifiableDocument
): DocumentVersion {

    const context = raw['@context'];

    // Parent version if no context
    if(!context && type === DocumentType.Endorsement && parent) {
        return parent.version;
    }

    if(isCowcertsV0(context))
        return DocumentVersion.CowcertsV0;

    if(isBlockcertsV2(context)) {
        return DocumentVersion.BlockcertsV2;
    }

    if(isBlockcertsV1(context)) {
        return DocumentVersion.BlockcertsV1;
    }

    throw Error(`No document version found`);
}
