import {VerifiableDocument} from '../../../models';
import getDocumentType from './getDocumentType';
import getExpirationDate from './getExpirationDate';
import getIssuer from './getIssuer';
import getName from './getName';
import getRevocationKey from './getRevocationKey';
import getSignature from './getSignature';
import getVerificationMap from './getVerificationMap';
import getVersion from "./getVersion";

export default function parseFromJson(
    raw: any,
    parentDocument?: VerifiableDocument,
    legacySteps: boolean = true
) {
    const type = getDocumentType(raw);
    const version = getVersion(raw, type, parentDocument);
    const name = getName(raw, type, version);
    const issuer = getIssuer(raw, type, version);
    const signature = getSignature(raw, version);
    const expires = getExpirationDate(raw);
    const revocationKey = getRevocationKey(raw);
    let anchor = signature.anchors[0];
    const steps = getVerificationMap(
        anchor.chain,
        anchor.otherChains,
        legacySteps
    );

    return new VerifiableDocument(
        raw,
        raw.id,
        type,
        version,
        name,
        issuer,
        signature,
        expires,
        revocationKey,
        parentDocument,
        steps
    );
}
