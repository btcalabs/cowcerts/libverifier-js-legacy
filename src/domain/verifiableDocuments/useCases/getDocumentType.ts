import * as _ from 'lodash';
import {DocumentType} from '../../../enums';

const CertificateTypeBlockcertsV1 = 'BlockchainCertificate';
const CertificateType = 'Assertion';
const EndorsementType = 'Endorsement';

export default function getDocumentType(raw: any): DocumentType {
    const jsonLdType = raw.type;

    // String JSONLD type
    if (_.isString(jsonLdType)) {
        if(jsonLdType === CertificateType ||
            jsonLdType === CertificateTypeBlockcertsV1)
            return DocumentType.Certificate;
        else if (jsonLdType === EndorsementType)
            return DocumentType.Endorsement;
    }

    // Array JSONLD type
    if(_.isArray(jsonLdType)) {
        if(jsonLdType.includes(CertificateType) ||
            jsonLdType.includes(CertificateTypeBlockcertsV1))
            return DocumentType.Certificate;
        else if (jsonLdType.includes(EndorsementType))
            return DocumentType.Endorsement;
    }

    // No type found
    throw new Error('No document type found');
}
