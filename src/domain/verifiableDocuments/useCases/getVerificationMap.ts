import {getSubStepEnumsFromStepGroupEnum} from '../../../constants';
import {StepGroupEnum} from "../../../enums";
import {Blockchain, OtherChain, StepGroup} from '../../../models';
import chains from '../../chains';
import steps from '../../steps';

const mockSubSteps = [
    ...getSubStepEnumsFromStepGroupEnum(StepGroupEnum.ContentValidation),
];

const endorsementSubSteps = [
    ...getSubStepEnumsFromStepGroupEnum(StepGroupEnum.EndorsementChecks),
];

const mainSubSteps = [
    ...getSubStepEnumsFromStepGroupEnum(StepGroupEnum.BlockchainRegistration),
    ...getSubStepEnumsFromStepGroupEnum(StepGroupEnum.AuthenticityChecks),
    ...getSubStepEnumsFromStepGroupEnum(StepGroupEnum.RevocationChecks),
    ...getSubStepEnumsFromStepGroupEnum(StepGroupEnum.ExpirationChecks),
];


/**
 * getVerificationMap
 *
 * Get verification map from the chain
 *
 * If legacy format is provided, will omit returning the group object in
 * each step.
 *
 * @param chain
 * @param otherChains
 * @param legacyFormat
 * @param isEndorsement
 * @returns {Array}
 */
export default function getVerificationMap(
    chain: Blockchain,
    otherChains: Array<OtherChain> = [],
    legacyFormat: boolean = true,
    isEndorsement: boolean = false,
): Array<StepGroup> {

    let subStepEnums = [];

    // No chain or otherChains provided
    if (!chain)
        return [];

    // Endorsement checks
    if (isEndorsement)
        subStepEnums = [...subStepEnums, ...endorsementSubSteps];

    // Basic mock steps
    subStepEnums = [...subStepEnums, ...mockSubSteps];

    // Main verification steps
    if (!chains.isMockChain(chain, otherChains)) {
        subStepEnums = [...subStepEnums, ...mainSubSteps];
    }

    return steps.getStepGroupsFromSubSteps(subStepEnums, legacyFormat);
}
