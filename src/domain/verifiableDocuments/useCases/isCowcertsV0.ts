import {CONTEXT_IRIS} from "../../../constants";
import * as _ from 'lodash';

export default function isCowcertsV0(context: any): boolean {

    // Has to be an array
    if (!_.isArray(context)) {
        return false;
    }

    // Has to contain a cowcerts context object.
    // Cowcerts context object: context object with 'cow' key and a cowcerts
    // context iri
    const COW_KEY = 'cow';
    for (let contextItem of context) {
        if (
            _.isObject(contextItem) &&
            _.has(contextItem, COW_KEY) &&
            contextItem[COW_KEY].includes(CONTEXT_IRIS.COWCERTS_V0)
        ) {
            return true;
        }
    }

    return false;
}
