import {BlockchainsList} from '../../../constants';
import {DocumentVersion} from "../../../enums";
import {
    Anchor,
    Blockchain,
    MerkleProof2019Signature,
    OtherChain,
} from '../../../models';
import domain from "../../index";
import * as _ from 'lodash';

/**
 * Finds a blockchain object given the string that appears in the signature
 * anchors' chain of a certificate.
 *
 * @param signatureValue
 */
function getBlockchainBySignatureValue(signatureValue: string): Blockchain {
    let blockchain = BlockchainsList.find(
        (blockchain) => blockchain.signatureValue == signatureValue
    );
    return blockchain;
}

function getOtherChain(otherChain: any): OtherChain {
    return new OtherChain(
        otherChain.id,
        otherChain.name,
        otherChain.protocol,
        otherChain.genesis,
        otherChain.consortium,
    )
}

function getAnchor(anchor: any, chain?: Blockchain): Anchor {

    const otherChains = anchor.otherChains ?
        anchor.otherChains.map(
            (otherChain) => getOtherChain(otherChain)) :
        undefined;

    return new Anchor(
        anchor.sourceId,
        chain ? chain : getBlockchainBySignatureValue(anchor.chain),
        anchor.type,
        otherChains,
    )
}

export default function getSignature(
    raw: any,
    version: DocumentVersion,
): MerkleProof2019Signature {

    let rawSignature = version === DocumentVersion.BlockcertsV1
        ? raw.receipt : raw.signature;

    let chain;
    if(version === DocumentVersion.BlockcertsV1) {
        const recipient = raw.recipient || raw.document.recipient;
        const publicKey = recipient.publicKey;
        chain = domain.certificates.getChain(publicKey);
    }
    else if(version === DocumentVersion.BlockcertsV2) {
        const issuerKey = _.get(raw, 'verification.publicKey', null) ||
            _.get(raw, 'verification.creator', null);
        if(issuerKey)
            chain = domain.certificates.getChain(issuerKey);
    }

    return new MerkleProof2019Signature(
        rawSignature.merkleRoot,
        rawSignature.targetHash,
        rawSignature.anchors.map((anchor) => getAnchor(anchor, chain)),
        rawSignature.proof
    );

}
