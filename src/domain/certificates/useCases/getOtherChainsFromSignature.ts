import { OtherChain } from '../../../models';

export default function getOtherChainsFromSignature (signature) {
  if (signature && signature.anchors instanceof Array) {
    let anchors = signature.anchors;
    let otherChains = [];
    for (let anchor of anchors) {
      // TODO: What if otherChains elements are not objects?
      if (anchor.otherChains instanceof Array) {
        otherChains = otherChains.concat(
          anchor.otherChains.map((otherChain) => new OtherChain(
            otherChain.id,
            otherChain.name,
            otherChain.protocol,
            otherChain.genesis,
            otherChain.consortium
          )));
      }
    }
    if (otherChains.length === 0) {
      return null;
    } else {
      return otherChains;
    }
  }
  return null;
}
