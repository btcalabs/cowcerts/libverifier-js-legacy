import {CERTIFICATE_VERSIONS} from '../../../constants';
import * as _ from 'lodash';

/**
 * Returns the expiration date as a string from a raw verifiable document.
 *
 * @param raw
 * @param version
 */
export default function getExpirationDate(
    raw: any,
    version: string = CERTIFICATE_VERSIONS.V2_0
): string {

    // Blockcerts version 1.x certificates
    if (
        version === CERTIFICATE_VERSIONS.V1_1 ||
        version === CERTIFICATE_VERSIONS.V1_2
    ) {
        return _.get(raw, 'document.assertion.expires', null);
    }

    // Blockcerts v2 and cowcerts
    return _.get(raw, 'expires', null);
}
