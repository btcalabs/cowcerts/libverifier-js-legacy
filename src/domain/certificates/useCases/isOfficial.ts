export default function isOfficial (certificateJson) {
  return !!certificateJson && !!certificateJson.badge &&
        !!certificateJson.badge.official;
}
