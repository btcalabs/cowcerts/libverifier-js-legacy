export default function getOfficialValidationEndorsement (certificate) {
  return certificate.officialValidation || null;
}
