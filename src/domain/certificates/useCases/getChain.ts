import {
    BitcoinMainnet, BitcoinTestnet, BlockchainsList,
} from '../../../constants';
import {Blockchain} from '../../../models';
import addresses from '../../addresses';
import {getText} from '../../i18n/useCases';

/**
 * Finds a blockchain object given the string that appears in the signature
 * anchors' chain of a certificate.
 *
 * @param signatureValue
 */
function getBlockchainBySignatureValue(signatureValue: string): Blockchain {
    return BlockchainsList
        .find(blockchain => blockchain.signatureValue == signatureValue)
}

/**
 * getChain
 *
 * Returns a chain object by looking at the signature value or the bitcoin
 * address (legacy)
 *
 * @param signature
 * @param address
 * @returns {*}
 */
export default function getChain(address, signature = null): Blockchain {
    let cleanedSignature = signature || {};
    if (cleanedSignature.anchors) {
        let anchors = cleanedSignature.anchors;
        let anchor = anchors[0];
        if (anchor.chain) {
            let chainObject = getBlockchainBySignatureValue(anchor.chain);
            if (typeof chainObject === 'undefined') {
                throw new Error(getText('errors', 'getChain'));
            }
            return chainObject;
        }
    }

    // Legacy path: we didn't support anything other than testnet and mainnet,
    // so we check the address prefix otherwise try to determine the chain from
    // a bitcoin address
    return addresses.isMainnet(address) ? BitcoinMainnet : BitcoinTestnet
}
