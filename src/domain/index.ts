import addresses from './addresses';
import certificates from './certificates';
import chains from './chains';
import i18n from './i18n';
import verifiableDocuments from './verifiableDocuments';

export default {
  addresses,
  certificates,
  chains,
  i18n,
  verifiableDocuments,
};
