import {SubStepsMap} from '../../../constants';
import {
    getStepGroupCode,
    getSubStepCode,
    StepGroupEnum,
    SubStepEnum,
} from "../../../enums";
import {StepGroup} from '../../../models';
import createStepGroup from "./createStepGroup";
import createSubStep from "./createSubStep";

function getStepGroupFromSubStepEnum(subStepEnum: SubStepEnum): StepGroupEnum {
    return Object.values(SubStepsMap)
        .find(subStepMap => subStepMap.steps.includes(subStepEnum))
        .group
}

function getStepGroupEnumsFromSubStepEnums(
    subStepEnums: Array<SubStepEnum>
): Array<StepGroupEnum> {
    return Array.from(new Set(
        subStepEnums.map(
            subStepEnum => getStepGroupFromSubStepEnum(subStepEnum)
        )
    ));
}

export default function getStepGroupsFromSubStepEnums(
    subStepEnums: Array<SubStepEnum>,
    legacyFormat: boolean
): Array<StepGroup> {

    // Find groups
    const stepGroupsEnum = getStepGroupEnumsFromSubStepEnums(subStepEnums);

    const stepGroups = stepGroupsEnum
        .map(stepGroup => createStepGroup(stepGroup));

    const subSteps = subStepEnums
        .map(subStep => createSubStep(subStep));

    // Link them
    subStepEnums.forEach((subStepEnum) => {

        // Find subStep
        const subStep = subSteps.find(subStep =>
            subStep.code === getSubStepCode(subStepEnum)
        );

        // Find stepGroup
        const stepGroupEnum = getStepGroupFromSubStepEnum(subStepEnum);
        const stepGroup = stepGroups.find(stepGroup =>
            stepGroup.code === getStepGroupCode(stepGroupEnum)
        );

        // Link them
        stepGroup.subSteps.push(subStep);
        if(legacyFormat) {
            subStep.parentStep = stepGroup.code;
        } else {
            subStep.group = stepGroup;
        }
    });

    return stepGroups;
}
