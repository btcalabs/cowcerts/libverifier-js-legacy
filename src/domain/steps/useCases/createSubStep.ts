import {SubStepEnum} from "../../../enums";
import SubStep from "../../../models/subStep";
import i18n from "../../i18n";

export default function createSubStep(subStep: SubStepEnum): SubStep {
    return new SubStep(
        subStep,
        i18n.getText('subSteps', `${subStep}Label`),
        i18n.getText('subSteps', `${subStep}LabelPending`)
    )
}
