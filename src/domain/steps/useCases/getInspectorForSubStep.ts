import {SubStepEnum} from '../../../enums';
import {
    CheckIfRevokedInspector,
    CheckIssuerKeysWereValidInspector,
    CompareHashWithSignatureInspector,
    CompareMerkleRootWithBlockchainInspector,
    CompareMerkleRootWithSignatureInspector,
    ComputeLocalHashInspector,
    ComputeMerkleRootInspector,
    EndorsementIsForDocumentInspector,
    FetchBlockchainMerkleRootInspector,
    FetchIssuerProfileInspector,
    FetchIssuerRevocationListInspector,
    CheckIfExpiredInspector,
} from "../../../inspectors";
import {Inspector, VerifiableDocument} from '../../../models';

interface SubStepToInspector {
   step: SubStepEnum,
   inspector: { new(...args: any[]): Inspector },
}

const SubStepToInspectors: Array<SubStepToInspector> = [
    {
        step: SubStepEnum.EndorsementIsForDocument,
        inspector: EndorsementIsForDocumentInspector
    },
    {
        step: SubStepEnum.ComputeHash,
        inspector: ComputeLocalHashInspector
    },
    {
        step: SubStepEnum.CompareHashWithSignature,
        inspector: CompareHashWithSignatureInspector
    },
    {
        step: SubStepEnum.ComputeMerkleRoot,
        inspector: ComputeMerkleRootInspector
    },
    {
        step: SubStepEnum.CompareMerkleRootWithSignature,
        inspector: CompareMerkleRootWithSignatureInspector
    },
    {
        step: SubStepEnum.FetchBlockchainMerkleRoot,
        inspector: FetchBlockchainMerkleRootInspector
    },
    {
        step: SubStepEnum.CompareMerkleRootWithBlockchain,
        inspector: CompareMerkleRootWithBlockchainInspector
    },
    {
        step: SubStepEnum.FetchIssuerProfile,
        inspector: FetchIssuerProfileInspector
    },
    {
        step: SubStepEnum.CheckIssuerKeysWereValid,
        inspector: CheckIssuerKeysWereValidInspector
    },
    {
        step: SubStepEnum.FetchIssuerRevocationList,
        inspector: FetchIssuerRevocationListInspector
    },
    {
        step: SubStepEnum.CheckIfRevoked,
        inspector: CheckIfRevokedInspector
    },
    {
        step: SubStepEnum.CheckIfExpired,
        inspector: CheckIfExpiredInspector
    }
];

export default function getInspectorForSubStep(
    subStep: SubStepEnum,
    doc: VerifiableDocument,
): Inspector {
    const subStepToInspector = SubStepToInspectors
        .find(subStepToInspector => subStepToInspector.step === subStep);

    if(!subStepToInspector)
        throw new Error(`No inspector for subStep ${subStep} found`);

    return new subStepToInspector.inspector(
        doc
    );

}
