import {StepGroupEnum} from "../../../enums";
import {StepGroup} from "../../../models";
import i18n from "../../i18n";

export default function createStepGroup(stepGroup: StepGroupEnum): StepGroup {
    return new StepGroup(
        stepGroup,
        i18n.getText('steps', `${stepGroup}Label`),
        i18n.getText('steps', `${stepGroup}LabelPending`)
    )
}
