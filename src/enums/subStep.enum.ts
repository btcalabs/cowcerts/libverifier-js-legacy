// By order of logical execution
enum SubStepEnum {

    // == ENDORSEMENT CHECKS ==
    EndorsementIsForDocument = 'EndorsementIsForDocument',

    // == CONTENT VALIDATION ==
    // TODO: JSON Schema validation using ajv
    // FormatValidation = 'FormatValidation',
    ComputeHash = 'ComputeHash',
    CompareHashWithSignature = 'CompareHashWithSignature',
    ComputeMerkleRoot = 'ComputeMerkleRoot',
    CompareMerkleRootWithSignature = 'CompareMerkleRootWithSignature',

    // == BLOCKCHAIN REGISTRATION ==
    FetchBlockchainMerkleRoot = 'FetchBlockchainMerkleRoot',
    CompareMerkleRootWithBlockchain = 'CompareMerkleRootWithBlockchain',

    // == AUTHENTICITY CHECKS ==
    FetchIssuerProfile = 'FetchIssuerProfile',
    CheckIssuerKeysWereValid = 'CheckIssuerKeysWereValid',

    // == REVOCATION CHECKS ==
    FetchIssuerRevocationList = 'FetchIssuerRevocationList',
    CheckIfRevoked = 'CheckIfRevoked',

    // == EXPIRATION CHECKS ==
    CheckIfExpired = 'CheckIfExpired',
}


/**
 * Returns the code name of a SubStep item
 */
function getSubStepCode(subStep: SubStepEnum): string {
    return subStep;
}

/**
 * Returns the codes of all SubSteps
 */
function getSubStepCodes(): Array<string> {
    return Object.values(SubStepEnum)
}

export {
    getSubStepCode,
    getSubStepCodes,
    SubStepEnum
}
