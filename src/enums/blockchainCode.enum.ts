enum BlockchainCode {
    BitcoinMainnet = 'bitcoin',
    EthereumMainnet = 'ethmain',
    EthereumRopsten = 'ethropst',
    Mocknet = 'mocknet',
    BitcoinRegtest = 'regtest',
    BitcoinTestnet = 'testnet',
}
export default BlockchainCode;
