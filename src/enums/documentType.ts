enum DocumentType {
    Certificate = 'Certificate',
    Endorsement = 'Endorsement'
}
export default DocumentType;
