enum BlockchainSignatureValue {
    BitcoinMainnet = 'bitcoinMainnet',
    EthereumMainnet = 'ethereumMainnet',
    EthereumRopsten = 'ethereumRopsten',
    Mockchain = 'mockchain',
    BitcoinRegtest = 'bitcoinRegtest',
    bitconTestnet = 'bitcoinTestnet',
}
export default BlockchainSignatureValue;
