enum StepGroupEnum {
    EndorsementChecks = 'EndorsementChecks',
    ContentValidation = 'ContentValidation',
    BlockchainRegistration = 'BlockchainRegistration',
    AuthenticityChecks = 'AuthenticityChecks',
    RevocationChecks = 'RevocationChecks',
    ExpirationChecks = 'ExpirationChecks',
    Final = 'final',
}

/**
 * Returns the code name of a step group enum item.
 */
function getStepGroupCode(stepGroup: StepGroupEnum): string {
    return stepGroup
}

/**
 * Returns all code names from the step group enum.
 */
function getStepGroupCodes(): Array<string> {
    return Object.values(StepGroupEnum)
}

export {
    StepGroupEnum,
    getStepGroupCode,
    getStepGroupCodes
}
