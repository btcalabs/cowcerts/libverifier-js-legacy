enum DocumentVersion {
    BlockcertsV1 = 'BlockcertsV1',
    BlockcertsV2 = 'BlockcertsV2',
    CowcertsV0 = 'CowcertsV0'
}
export default DocumentVersion;
