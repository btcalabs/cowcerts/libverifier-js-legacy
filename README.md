# DEPRECATION NOTICE: This verifier is not being maintained any more and will be replaced soon by [@cowcerts/libverifier-js](https://gitlab.com/cowcerts/libverifier-js)

# @cowcerts/libverifier-js

A library to parse and verify [Cowcerts][cowcerts] certificates.

Based on [Blockcerts verifier library][cert-verifier-js].

# Usage

## Install

```shell
npm i @cowcerts/libverifier-js
```

## Import

#### Commonjs
Exposed by default:

```javascript
var Certificate = require('@cowcerts/libverifier-js');
var certificate = new Certificate(certificateDefinition);
```

#### ES module
```javascript
import { Certificate } from '@cowcerts/libverifier-js';
let certificate = new Certificate(certificateDefinition);
```

#### Script tag (iife)
[Check an exampl here](test/e2e/script-tag.html)
```html
<script src='node_modules/@cowcerts/libverifier-js/dist/verifier-iife.js'></script>
<script>
  var certificate = new Verifier.Certificate(certificateDefinition);
</script>
```

## Examples

You can find more examples in the [test folder](./test/e2e/).

### Parse a [Cowcerts][cowcerts] certificate

```javascript
var fs = require('fs');

fs.readFile('./certificate.json', 'utf8', function (err, data) {
  if (err) {
    console.log(err);
  }

  let certificate = new Certificate(data);
  console.log(cert.name);
});
```

### Verify a [Cowcerts][cowcerts] certificate

```javascript
var fs = require('fs');

fs.readFile('./certificate.json', 'utf8', function (err, data) {
  if (err) {
    console.log(err);
  }

  let certificate = new Certificate(data);
  const verificationResult = await certificate.verify(({code, label, status, errorMessage}) => {
    console.log('Code:', code, label, ' - Status:', status);
    if (errorMessage) {
      console.log(`The step ${code} fails with the error: ${errorMessage}`);
    }
  });

  if (verificationResult.status === 'failure') {
    console.log(`The certificate is not valid. Error: ${verificationResult.errorMessage}`);
  }
});
```

# API

## Compatibility with [Blockcerts][blockcerts]
This library's API is compatible with [Blockcerts'][blockcerts]
[cert-verifier-js][cert-verifier-js], so you can just use it as if you were
using that library, but with the extra [Cowcerts][cowcerts] features.

### Changes respect the [Blockcerts cert-verifier-js][cert-verifier-js]
This is the list of changes that the [Cowcerts][cowcerts] verifier library 
adds:
 - Certificate `signatureImage` field now contains also the [signature 
 lines][signature-line] in the [root of the certificate (assertion)][cowcerts-assertion].
 - Certificate `otherChains` field with all the anchors in other chains.


[cowcerts-assertion]: https://cowcerts.readthedocs.io/en/v0/edu/specs/jsonld.html#assertion 

[signature-line]: https://github.com/blockchain-certificates/cert-schema/blob/master/docs/signatureLineExtension_schema.md

## `Certificate`

### `new Certificate(certificateDefinition, options)`
```javascript
const certificate = new Certificate(certificateDefinition);
```
The constructor automatically parses a certificate.

#### Parameter
- `certificateDefinition` (`String|Object`): the certificate definition. Can either be a string or a JSON object.
- `options`: (`Object`): an object of options. The following properties are used:
    - locale: (`String`): language code used to set the language used by the verifier. Default: `en-US`.

#### Returns
The certificate instance has the following properties:
- `certificateImage`: `String`. Raw data of the certificate image
- `certificateJson`: `Object`. Certificate JSON object
- `chain`: `Object`. Chain the certificate was issued on
- `description`: `String`. Description of the certificate
- `expires`: `String`. Expiration date
- `id`: `String`. Certificate's ID
- `isFormatValid`: `Boolean`. Indicates whether or not the certificate has a valid format
- `issuedOn`: `String`. Datetime of issuance (ISO-8601)
- `issuer`: `Object`. Certificate issuer
- `locale`: `String`. Language code used by the verifier
- `metadataJson`: `Object`. Certificate metadata object
- `name`: `String`. Name of the certificate
- `publicKey`: `String`. Certificate's public key
- `receipt`: `String`. Certificate's receipt
- `recipientFullName`: `String`. Full name of recipient
- `recordLink`: `String`. Link to the certificate record
- `revocationKey`: `String|null`. Revocation key (if any)
- `sealImage`: `String`. Raw data of the seal's image;
- `signature`: `String`. Certificate's signature
- `signatureImage`: [`SignatureImage[]`][signatureLineModel]. Array of certificate [signature lines][signatureLineModel]*.
- `subtitle`: `String`. Subtitle of the certificate
- `transactionId`: `String`. Transaction ID
- `rawTransactionLink`: `String`. Raw transaction ID
- `transactionLink`: `String`. Transaction link
- `verificationSteps`: `VerificationStep[]`. The array of steps the certificate will have to go through during verification
- `version`: `CertificateVersion`: [Blockcerts version of the certificate](src/constants/certificateVersions.js)
- `otherChains`: [`OtherChain[]`][otherchain]. Array of other _blockchains_ to query for anchors of the certificate.
- `endorsements`: [`Endorsement[]`][endorsement]. Array of [endorsement][endorsement]s that add information about this certificate and will also be verified.
- `endorsementsMap`: `Object`. Map of the previous `endorsements`, where the key a `string` containing the endorsement claim type and the value is the [endorsement][endorsement] object [`Endorsement`][endorsement].
- `recipientClaim`: [`RecipientClaim`][recipient-claim]. Recipient claim updating the available recipient information and validated by a third party (just present if an endorsement with type `RecipientClaim` is found).

[signatureLineModel]: src/models/signatureImage.js
[otherchain]: https://cowcerts.readthedocs.io/en/v0/edu/specs/jsonld.html#other-chain
[endorsement]: https://cowcerts.readthedocs.io/en/v0/edu/specs/jsonld.html#endorsement
[recipient-claim]: https://cowcerts.readthedocs.io/en/v0/edu/specs/jsonld.html#recipientclaim

**Note:** `verificationSteps` is generated according to the nature of the certificate. The full steps array is provided ahead of verification in order to give more flexibility to the consumer. For example, you might want to pre-render the verification steps for animation, or render a count of steps and/or sub-steps.

`VerificationStep` has the following shape:
```javascript
{
    code: 'formatValidation',
    label: 'Format validation',
    labelPending: 'Validating format',
    subSteps: [
      {
        code: 'getTransactionId',
        label: 'Get transaction ID',
        labelPending: 'Getting transaction ID',
        parentStep: 'formatValidation'
      },
      ...
    ]
}
```

### `verify(stepCallback)`
This will run the verification of a certificate. The function is asynchronous.

```javascript
const certificateVerification = await certificate.verify(({code, label, status, errorMessage}) => {
    console.log('Sub step update:', code, label, status);
}));
console.log(`Verification was a ${certificateVerification.status}:`, certificateVerification.errorMessage);
```

#### Parameters
- `({code, label, status, errorMessage}) => {}` (`Function`): callback function called whenever a substep status has changed. The callback parameter has 4 properties:
  - `code`: substep code
  - `label`: readable label of the substep
  - `status`: substep status (`success`, `failure`, `starting`)
  - `errorMessage`: error message (optional)

#### Returns
The final verification status:
```javascript
{ code, status, errorMessage }
```
- `code`: code of the final step (`final`)
- `status`: final verification status (`success`, `failure`)
- `errorMessage`: error message (optional)

### Constants
Several constants are being exposed:
```javascript
import { BLOCKCHAINS, STEPS, SUB_STEPS, CERTIFICATE_VERSIONS,
VERIFICATION_STATUSES } from '@cowcerts/cert-verifier-js';
```
- [`BLOCKCHAINS`](https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/blockchains.js): descriptive object of all blockchains supported by the library
- [`STEPS`](https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/verificationSteps.js): descriptive object of all verification steps (top level)
- [`SUB_STEPS`](https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/verificationSubSteps.js): descriptive object of all verification substeps
- [`CERTIFICATE_VERSIONS`](https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/certificateVersions.js): list of all certificate versions
- [`VERIFICATION_STATUSES`](https://github.com/blockchain-certificates/cert-verifier-js/blob/master/src/constants/verificationStatuses.js)

### i18n
The exposed function `getSupportedLanguages()` returns an array of language codes supported by the library.
```javascript
import { getSupportedLanguages } from '@cowcerts/cert-verifier-js';
getSupportedLanguages(); // ['en-US', 'es-ES', 'mt', ...]
```
You can use the codes for the `locale` option.

Please note that while we are working to add new languages, any new translation is welcome through forking & PR.

## Contribute

### Run the tests
```shell
npm run test
```

### Build
```shell
npm run build
```
The build files are in the `dist` folder.

## Verification process
If you want more details about the verification process, please check out the
 [documentation][cowcerts-validation].

 **\<\> with ♥ by [BTC Assessors](https://btcassessors.com):
 [@davidlj95](https://gitlab.com/davidlj95), [@ccebrecos](https://gitlab.com/ccebrecos)**

 **Based on the work of [Blockcerts' cert-verifier-js][cert-verifier-js]**

 [![BTC Assessors](https://i.imgur.com/7nzUvR0.png)](https://www.btcassessors.com)


[blockcerts]: https://www.blockcerts.org
[cowcerts]: https://www.cowcerts.org
[cowcerts-validation]: https://cowcerts.readthedocs.io/en/stable/edu/specs/validation.html
[cert-verifier-js]: https://github.com/blockchain-certificates/cert-verifier-js
