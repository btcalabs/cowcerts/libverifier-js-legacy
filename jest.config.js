module.exports = {
  preset: 'ts-jest/presets/js-with-babel',
  setupFiles: [
    '<rootDir>/node_modules/regenerator-runtime/runtime'
  ]
};
