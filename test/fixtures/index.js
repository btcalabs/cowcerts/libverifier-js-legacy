import EthereumMainV2Valid from './ethereum-main-valid-2.0';
import EthereumMainInvalidMerkleRoot from './ethereum-merkle-root-unmatch-2.0';
import EthereumMainRevoked from './ethereum-revoked-2.0';
import EthereumRopstenV2Valid from './ethereum-ropsten-valid-2.0';
import EthereumTampered from './ethereum-tampered-2.0';
import MainnetInvalidMerkleReceipt from './mainnet-invalid-merkle-receipt-2.0';
import MainnetMerkleRootUmmatch from './mainnet-merkle-root-unmatch-2.0';
import MainnetV2Revoked from './mainnet-revoked-2.0';
import MainnetV2Valid from './mainnet-valid-2.0';
import MainnetV2AlphaValid from './mainnet-valid-2.0-alpha';
import MocknetV2Valid from './mocknet-valid-2.0';
import RegtestV2Valid from './regtest-valid-2.0';
import TestnetV1IssuerUrl404 from './testnet-404-issuer-url-1.2';
import TestnetV1NoIssuerProfile from './testnet-no-issuer-profile-1.2';
import TestnetRevokedV2 from './testnet-revoked-key-2.0';
import TestnetTamperedHashes from './testnet-tampered-hashes-2.0';
import TestnetV1Valid from './testnet-valid-1.2';
import TestnetV2Valid from './testnet-valid-2.0';
import TestnetV2ValidV1Issuer from './testnet-valid-v1-issuer-2.0';
import CowcertsMockValid from './cowcerts-mock-valid';
import CowcertsMockValidWithoutEDS from './cowcerts-mock-valid-without-eds';
import CowcertsMockInvalidWrongOfficializationClaimId from './cowcerts-mock-invalid-wrong-officialization-claim-id';
import CowcertsMockInvalidWrongRecipientClaimId from './cowcerts-mock-invalid-wrong-recipient-claim-id';
import CowcertsMockInvalidWrongEDSClaimId from './cowcerts-mock-invalid-wrong-eds-claim-id';
import CowcertsValidBlockValleyWithEDS from './cowcerts-valid-blockvalley-with-eds';

export default {
  EthereumMainV2Valid,
  EthereumMainInvalidMerkleRoot,
  EthereumMainRevoked,
  EthereumRopstenV2Valid,
  EthereumTampered,
  MainnetInvalidMerkleReceipt,
  MainnetMerkleRootUmmatch,
  MainnetV2Revoked,
  MainnetV2Valid,
  MainnetV2AlphaValid,
  MocknetV2Valid,
  RegtestV2Valid,
  TestnetV1IssuerUrl404,
  TestnetV1NoIssuerProfile,
  TestnetRevokedV2,
  TestnetTamperedHashes,
  TestnetV1Valid,
  TestnetV2Valid,
  TestnetV2ValidV1Issuer,
  CowcertsMockValid,
  CowcertsMockValidWithoutEDS,
  CowcertsMockInvalidWrongOfficializationClaimId,
  CowcertsMockInvalidWrongRecipientClaimId,
  CowcertsMockInvalidWrongEDSClaimId,
  CowcertsValidBlockValleyWithEDS
};
