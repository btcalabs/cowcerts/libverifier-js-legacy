import * as _ from 'lodash';
import CowcertsCertificateCowcertsV0Parser
    from '../../../../src/parser/cowcerts/cowcerts-v0';
import fixture
    from '../../../fixtures/cowcerts-valid-blockvalley-with-eds.json';

describe('Cowcerts v0: Cowcerts Parser test suite', function () {
    describe('given it is called with valid v2 certificate data', function () {
        let parsedCertificate;

        beforeEach(function () {
            parsedCertificate = new CowcertsCertificateCowcertsV0Parser().parse(fixture);
        });

        it('should find attached endorsements of the certificate object', function () {
            expect(parsedCertificate.endorsements.length).toEqual(2);
        });

        it('should create an endorsements map with to keys', function () {
            expect(Object.keys(parsedCertificate.endorsementsMap).length).toEqual(2);
        });

        it('should place the RecipientClaim in the endorsements map', function () {
            let recipientClaim = _.find(fixture.signature.endorsements,
                (endorsement) => endorsement.claim.type.includes('RecipientClaim'));
            expect(parsedCertificate.endorsementsMap['RecipientClaim']).toEqual(recipientClaim);
        });

        it('should place the EDSClaim in the endorsements map', function () {
            let recipientClaim = _.find(fixture.signature.endorsements,
                (endorsement) => endorsement.claim.type.includes('EDSClaim'));
            expect(parsedCertificate.endorsementsMap['EDSClaim']).toEqual(recipientClaim);
        });

        it('should place the recipient claim', function () {
            let recipientEndorsement = _.find(fixture.signature.endorsements,
                (endorsement) => endorsement.claim.type.includes('RecipientClaim'));
            let recipientClaim = JSON.parse(JSON.stringify(recipientEndorsement.claim));
            delete recipientClaim.type;
            expect(parsedCertificate.recipientClaim).toEqual(recipientClaim);
        });


    });
});
