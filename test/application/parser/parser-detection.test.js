import BlockcertsCertificateBlockcertsV1Parser from '../../../src/parser/blockcerts/blockcerts-v1';
import BlockcertsCertificateBlockcertsV2Parser from '../../../src/parser/blockcerts/blockcerts-v2';
import BlockcertsCertificateCowcertsV0Parser from '../../../src/parser/blockcerts/cowcerts-v0';
import parseJSON from '../../../src/parser/index';
import BlockcertsV1Fixture from '../../fixtures/testnet-valid-1.2';
import BlockcertsV2Fixture from '../../fixtures/mainnet-valid-2.0';
import CowcertsV0Fixture from '../../fixtures/cowcerts-mock-valid';

describe('Parser suite: detect proper standard to parse', function () {
  it('should detect it\'s a Blockcerts v1 certificate', function () {
    let parseSpy = jest.spyOn(BlockcertsCertificateBlockcertsV1Parser.prototype, 'parse');
    parseJSON(BlockcertsV1Fixture);
    expect(parseSpy).toHaveBeenCalled();
  });
  it('should detect it\'s a Blockcerts v2 certificate', function () {
    let parseSpy = jest.spyOn(BlockcertsCertificateBlockcertsV2Parser.prototype, 'parse');
    parseJSON(BlockcertsV2Fixture);
    expect(parseSpy).toHaveBeenCalled();
  });
  it('should detect it\'s a Cowcerts v0 certificate', function () {
    let parseSpy = jest.spyOn(BlockcertsCertificateCowcertsV0Parser.prototype, 'parse');
    parseJSON(CowcertsV0Fixture);
    expect(parseSpy).toHaveBeenCalled();
  });
  it('should fail on exception', function () {
    let wrongCert = {};
    expect(() => parseJSON(wrongCert)).toThrow('Invalid certificate format');
  });
});
