import { StepGroupEnum } from '../../../../../src/enums';
import { getStepGroupWithSubSteps } from './helpers';

export default [
  getStepGroupWithSubSteps(StepGroupEnum.ContentValidation)
];
