import { StepGroupEnum } from '../../../../../src/enums';
import { getStepGroupWithSubSteps } from './helpers';

export default [
  getStepGroupWithSubSteps(StepGroupEnum.ContentValidation),
  getStepGroupWithSubSteps(StepGroupEnum.BlockchainRegistration),
  getStepGroupWithSubSteps(StepGroupEnum.AuthenticityChecks),
  getStepGroupWithSubSteps(StepGroupEnum.RevocationChecks),
  getStepGroupWithSubSteps(StepGroupEnum.ExpirationChecks)
];
