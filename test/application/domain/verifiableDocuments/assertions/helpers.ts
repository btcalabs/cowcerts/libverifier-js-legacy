import { STEPS, SUB_STEPS } from '../../../../../src';
import {StepGroupEnum} from "../../../../../src/enums";
import {StepGroup} from "../../../../../src/models";

export function getStepGroupWithSubSteps (stepGroupEnum: StepGroupEnum): Array<StepGroup> {
  return{
    ...STEPS.language[stepGroupEnum],
    subSteps: Object.keys(SUB_STEPS)
      .filter(subStepCode => subStepCode !== 'language')
      .filter(
        subStepCode => SUB_STEPS.language[subStepCode].parentStep === stepGroupEnum)
      .map(subStepCode => SUB_STEPS.language[subStepCode])
  };
}
