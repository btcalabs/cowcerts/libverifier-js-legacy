import {DocumentVersion} from '../../../../../src/enums';
import verifiableDocuments from '../../../../../src/domain/verifiableDocuments';
import blockcertsV1CertificateFixture
    from '../../../../fixtures/testnet-valid-1.2.json';
import blockcertsV2CertificateFixture
    from '../../../../fixtures/mainnet-valid-2.0.json';
import cowcertsV0CertificateFixture
    from '../../../../fixtures/cowcerts-valid-blockvalley-with-eds.json';

describe('Verifiable documents test suite', () => {
    describe('given a valid certificate', () => {
        it('should detect it is Blockcerts version 1', () => {
            const result =
                verifiableDocuments.getVersion(blockcertsV1CertificateFixture);
            expect(result).toBe(DocumentVersion.BlockcertsV1);
        });
        it('should detect it is Blockcerts version 2', () => {
            const result =
                verifiableDocuments.getVersion(blockcertsV2CertificateFixture);
            expect(result).toBe(DocumentVersion.BlockcertsV2);
        });
        it('should detect it is Cowcerts version 0', () => {
            const result =
                verifiableDocuments.getVersion(cowcertsV0CertificateFixture);
            expect(result).toBe(DocumentVersion.CowcertsV0);
        });

    });
});
