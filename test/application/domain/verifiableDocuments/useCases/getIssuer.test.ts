import {DocumentType,DocumentVersion} from '../../../../../src/enums';
import verifiableDocuments from '../../../../../src/domain/verifiableDocuments';
import certificateFixture
    from '../../../../fixtures/cowcerts-valid-blockvalley-with-eds.json';
import {getEDSEndorsementFromFixture} from "./helpers";

const edsEndorsement = getEDSEndorsementFromFixture(certificateFixture);

describe('Verifiable documents test suite', () => {
    describe('given a valid certificate', () => {
        it('should get the issuer', () => {
            expect(verifiableDocuments.getIssuer(
                certificateFixture,
                DocumentType.Certificate,
                DocumentVersion.CowcertsV0
            )).toBe(certificateFixture.badge.issuer);
        });
    });
    describe('given a valid endorsement', () => {
        it('should get the issuer', () => {
            expect(verifiableDocuments.getIssuer(
                edsEndorsement,
                DocumentType.Endorsement,
                DocumentVersion.CowcertsV0
            )).toBe(edsEndorsement.issuer);
        });
    });
});
