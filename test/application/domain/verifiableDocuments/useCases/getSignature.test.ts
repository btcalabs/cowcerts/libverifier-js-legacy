import verifiableDocuments from '../../../../../src/domain/verifiableDocuments';
import DocumentVersion from "../../../../../src/enums/documentVersion";
import certificateFixture
    from '../../../../fixtures/cowcerts-valid-blockvalley-with-eds.json';
import {createSignatureFixture, getEDSEndorsementFromFixture} from "./helpers";

const edsEndorsement = getEDSEndorsementFromFixture(certificateFixture);

describe('Verifiable documents test suite', () => {
    describe('given a valid certificate', () => {
        it('should get the signature', () => {
            expect(
                verifiableDocuments.getSignature(
                    certificateFixture,
                    DocumentVersion.CowcertsV0,
                    )
            ).toEqual(
                createSignatureFixture(certificateFixture)
            );
        });
    });
    describe('given a valid endorsement', () => {
        it('should get the signature', () => {
            expect(
                verifiableDocuments.getSignature(
                    edsEndorsement,
                    DocumentVersion.CowcertsV0,
                )
            ).toEqual(
                createSignatureFixture(edsEndorsement)
            );
        })
    });
});
