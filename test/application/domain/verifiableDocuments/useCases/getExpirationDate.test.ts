import verifiableDocuments from '../../../../../src/domain/verifiableDocuments';

describe('Verifiable documents test suite', () => {
    describe('given a valid certificate or endorsement', () => {
        it('should set expiration date to null if no date is present', () => {
            expect(verifiableDocuments.getExpirationDate({})).toBe(null)
        });
        it('should set expiration date if present', () => {
            let fixtureDateString = '2016-12-31T23:59:59+00:00';
            let fixtureDate = new Date(
                Date.UTC(2016, 11, 31, 23, 59, 59, 0));
            expect(
                verifiableDocuments.getExpirationDate(
                    {expires: fixtureDateString})
            ).toEqual(fixtureDate)
        });
    });
});
