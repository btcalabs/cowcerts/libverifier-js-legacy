import {DocumentType} from '../../../../../src/enums';
import verifiableDocuments from '../../../../../src/domain/verifiableDocuments';
import certificateFixture
    from '../../../../fixtures/cowcerts-valid-blockvalley-with-eds.json';
import {getEDSEndorsementFromFixture} from "./helpers";

const edsEndorsement = getEDSEndorsementFromFixture(certificateFixture);

describe('Verifiable documents test suite', () => {
    describe('given a valid certificate', () => {
        it('should detect if type is a certificate', () => {
            expect(
                verifiableDocuments.getDocumentType(certificateFixture)
            ).toBe(DocumentType.Certificate);
        });
    });
    describe('given a valid endorsement', () => {
        it('should detect if type is a endorsement', () => {
            const result = verifiableDocuments.getDocumentType(edsEndorsement);
            expect(result).toBe(DocumentType.Endorsement);
        });
    });
});
