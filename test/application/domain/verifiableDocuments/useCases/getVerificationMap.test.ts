import {StepGroupEnum} from "../../../../../src/enums";
import domain from '../../../../../src/domain';
import {BLOCKCHAINS} from '../../../../../src/index';
import {OtherChain} from "../../../../../src/models";
import certificateMainMapAssertion
    from '../assertions/certificateMainMapAssertion';
import certificateMockMapAssertion
    from '../assertions/certificateMockMapAssertion';
import {getStepGroupWithSubSteps} from "../assertions/helpers";

describe('Verifiable documents test suite', function () {
    describe('given a chain without otherChains', function () {
        describe('given it is called with the mocknet chain', function () {
            it('should return a mocknet verification map', function () {
                const result = domain.certificates.getVerificationMap(BLOCKCHAINS.mocknet);
                expect(result).toEqual(certificateMockMapAssertion);
            });
        });

        describe('given it is called with the bitcoin chain', function () {
            it('should return a mainnet verification map', function () {
                const result = domain.certificates.getVerificationMap(BLOCKCHAINS.bitcoin);
                expect(result).toEqual(certificateMainMapAssertion);
            });
        });

        describe('given it is called without a chain and a version', function () {
            it('should return an empty array', function () {
                // @ts-ignore
                const result = domain.certificates.getVerificationMap();
                expect(result).toEqual([]);
            });
        });
    });
    describe('given a chain with otherChains', function () {
        describe('given the chain is mocknet but an otherChain is present', function () {
            it('should return a mainnet verification map', function () {
                const otherChainFixture = new OtherChain(
                    'https://consortium.example.org/chain/eth/prod',
                    'Example Consortium Ethereum Chain',
                    'ETH',
                    'd4e56740f876aef8c010b86a40d5f56745a118d0906a34e69aec8c0db1cb8fa3',
                    'https://consortium.example.org'
                );
                const result = domain.certificates.getVerificationMap(
                    BLOCKCHAINS.bitcoin,
                    [otherChainFixture]
                );
                expect(result).toEqual(certificateMainMapAssertion);
            });
        });
    });
    describe('given the document is an endorsement', function () {
        it('should add the endorsement checks step group', function () {
            const result = domain.certificates.getVerificationMap(
                BLOCKCHAINS.bitcoin,
                [],
                true,
                true,
            );
            expect(result).toContainEqual(getStepGroupWithSubSteps(
                StepGroupEnum.EndorsementChecks))
        });
    });
    describe('given legacy format flag is enabled', function () {
        it('should not contain the group attribute in each substep', function () {
            const result = domain.certificates.getVerificationMap(
                BLOCKCHAINS.bitcoin,
                [],
                true,
            );
            const subSteps = result
                .map(stepGroup => stepGroup.subSteps)
                .flat()
            subSteps.forEach(subStep => expect(subStep).not.toHaveProperty('_group'));
        })
    });
    describe('given legacy format flag is disabled', function () {
        it('should contain the group attribute in each substep', function () {
            const result = domain.certificates.getVerificationMap(
                BLOCKCHAINS.bitcoin,
                [],
                false,
            );
            const subSteps = result
                .map(stepGroup => stepGroup.subSteps)
                .flat();
            subSteps.forEach(subStep => expect(subStep).toHaveProperty('_group'));
        })
    });
});
