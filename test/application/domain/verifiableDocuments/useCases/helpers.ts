import {BlockchainsList} from '../../../../../src/constants';
import {Blockchain, MerkleProof2019Signature} from '../../../../../src/models';

function getBlockchainBySignatureValue(signatureValue: string): Blockchain {
    return BlockchainsList
        .find(blockchain => blockchain.signatureValue == signatureValue)
}

export function getEDSEndorsementFromFixture(fixture: any) {
    return fixture.signature.endorsements
        .find(endorsement => endorsement.claim.type.includes('EDSClaim'));
}

export function createSignatureFixture(raw: any): MerkleProof2019Signature {
    let fixtureSignature = JSON.parse(JSON.stringify(raw.signature));
    fixtureSignature.endorsements = [];
    delete fixtureSignature.type;
    fixtureSignature.anchors[0].chain = getBlockchainBySignatureValue(
        fixtureSignature.anchors[0].chain
    );
    return new MerkleProof2019Signature(
        fixtureSignature.merkleRoot,
        fixtureSignature.targetHash,
        fixtureSignature.anchors,
        fixtureSignature.proof,
        []
    )
}
