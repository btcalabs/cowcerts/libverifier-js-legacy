import {getInspectorForSubStep}
    from '../../../../../src/domain/steps/useCases';
import {SubStepEnum} from "../../../../../src/enums";

describe('domain steps inspector mapping test suite', function () {
    for (let substep in SubStepEnum) {
        describe(`given the ${substep} substep`, function () {
            it(`should return an inspector`, function () {
                // @ts-ignore
                const result = getInspectorForSubStep(substep);
                expect(result).toBeTruthy();
            });
        });
    }
    describe('given an invalid subStep', function () {
        it('should throw an error', function () {
            expect(
                // @ts-ignore
                () => getInspectorForSubStep('NonExistentSubStep')
            ).toThrow()
        });
    })
});
